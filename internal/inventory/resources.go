/*
Copyright 2024 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package inventory

import (
	"context"
	"log"
	"sync"

	"github.com/pkg/errors"
	"golang.org/x/sync/semaphore"
	corev1 "k8s.io/api/core/v1"
	eventv1 "k8s.io/api/events/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/sets"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

type ResourcesMap map[types.UID]*unstructuredNode

func GetResourcesMap(ctx context.Context, namespaces []string, clientConfig *rest.Config) (ResourcesMap, error) {
	resMap := ResourcesMap{}

	for _, ns := range namespaces {
		nsResources, err := GetNamespaceResources(ctx, clientConfig, ns)
		if err != nil {
			return nil, err
		}
		for _, obj := range nsResources {
			// intended for future debug log option
			// if _, found := resMap[obj.GetUID()]; found {
			// 	log.Printf(
			// 		"ResourcesMap already contains an objects with uid %s, %s %s/%s won't be added",
			// 		obj.GetUID(),
			// 		obj.GroupVersionKind(),
			// 		obj.GetNamespace(),
			// 		obj.GetName(),
			// 	)
			// }
			resMap[obj.GetUID()] = &unstructuredNode{Unstructured: obj.DeepCopy()}
		}
	}
	for _, res := range resMap {
		for _, ownerRef := range res.GetOwnerReferences() {
			if owner, found := resMap[ownerRef.UID]; found {
				owner.Owned = append(owner.Owned, res)
			}
		}
		if res.GroupVersionKind().Kind == "Event" && res.GroupVersionKind().Group == eventv1.GroupName {
			event := eventv1.Event{}
			if err := runtime.DefaultUnstructuredConverter.FromUnstructured(res.UnstructuredContent(), &event); err != nil {
				log.Printf("Failed to convert Event: %s\n", err)
			}
			if involvedObject, found := resMap[event.Regarding.UID]; found {
				involvedObject.Events = append(involvedObject.Events, &event)
			}
		}
	}
	return resMap, nil
}

func GetNamespaceResources(ctx context.Context, clientConfig *rest.Config, namespace string) ([]unstructured.Unstructured, error) {

	grs, err := groupResources(clientConfig)
	if err != nil {
		return nil, errors.Wrap(err, "fetch available group resources")
	}
	return fetchResources(ctx, clientConfig, namespace, grs)
}

func groupResources(clientConfig *rest.Config) ([]schema.GroupVersionResource, error) {
	client, err := discovery.NewDiscoveryClientForConfig(clientConfig)
	if err != nil {
		return nil, errors.Wrap(err, "discovery client")
	}

	resources, err := client.ServerPreferredResources()
	if err != nil {
		log.Printf("Could not fetch list of API resources")
	}

	var grs []schema.GroupVersionResource
	for _, list := range resources {
		if len(list.APIResources) == 0 {
			continue
		}
		gv, err := schema.ParseGroupVersion(list.GroupVersion)
		if err != nil {
			continue
		}
		for _, r := range list.APIResources {
			if len(r.Verbs) == 0 {
				continue
			}

			if !r.Namespaced {
				// We won't search for cluster-scoped resources for now
				continue
			}

			// filter to resources that can be listed
			if !sets.NewString(r.Verbs...).HasAny("list", "get") {
				continue
			}
			// filter out v1/Events, as they overlap with events.k8s.io/v1/Events
			// as both share the same UID, they would override each other in resourceMap
			if gv.Group == corev1.GroupName && r.Name == "events" {
				continue
			}

			grs = append(grs, schema.GroupVersionResource{
				Group:    gv.Group,
				Version:  gv.Version,
				Resource: r.Name,
			})
		}
	}
	return grs, nil
}

func fetchResources(ctx context.Context, clientConfig *rest.Config, namespace string, grs []schema.GroupVersionResource) ([]unstructured.Unstructured, error) {
	client, err := dynamic.NewForConfig(clientConfig)
	if err != nil {
		return nil, err
	}
	sem := semaphore.NewWeighted(64) // restrict parallelism to 64 inflight requests

	var mu sync.Mutex
	var res []unstructured.Unstructured

	var wg sync.WaitGroup
	for _, gr := range grs {
		wg.Add(1)
		go func(gvr schema.GroupVersionResource) {
			defer wg.Done()
			if err := sem.Acquire(ctx, 1); err != nil {
				return // context cancelled
			}
			defer sem.Release(1)
			resp, err := client.Resource(gvr).Namespace(namespace).List(ctx, metav1.ListOptions{})
			if err != nil {
				log.Printf("Cannot fetch: %v", err)
				return
			}
			mu.Lock()
			res = append(res, resp.Items...)
			mu.Unlock()
		}(gr)
	}
	wg.Wait()
	return res, nil
}
