/*
Copyright 2024 The Sylva authors


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package inventory

import (
	"context"
	"fmt"
	"io"
	"log"
	"sort"
	"strings"
	"text/tabwriter"
	"time"

	eventv1 "k8s.io/api/events/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/rest"
	kstatus "sigs.k8s.io/cli-utils/pkg/kstatus/status"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/tree"
)

func PrintResourcesStatus(ctx context.Context, opts InventoryOptions, clientConfig *rest.Config, objs ...client.Object) {
	var out strings.Builder
	w := tabwriter.NewWriter(io.Writer(&out), 0, 0, 1, ' ', 0)
	fmt.Fprintln(w, header)
	for _, obj := range objs {
		status, err := formatResourceStatus(ctx, opts, obj, clientConfig)
		if err != nil {
			log.Printf("Failed to generate status for %s %s: %s", obj.GetObjectKind().GroupVersionKind().Kind, client.ObjectKeyFromObject(obj), err)
			continue
		}
		fmt.Fprint(w, status)
	}
	w.Flush()
	// Trim trailing spaces as text/tabwriter doesn't support that (see https://github.com/golang/go/issues/5994)
	lines := strings.Split(out.String(), "\n")
	for i := range lines {
		lines[i] = strings.TrimRight(lines[i], " ")
	}
	fmt.Print(strings.Join(lines, "\n"))
}

func formatResourceStatus(ctx context.Context, opts InventoryOptions, obj client.Object, clientConfig *rest.Config) (string, error) {

	kubeClient, err := GetClient(clientConfig)
	if err != nil {
		return "", err
	}

	objName := client.ObjectKeyFromObject(obj).String()
	objKind, err := kubeClient.GroupVersionKindFor(obj)
	if err != nil {
		return "", fmt.Errorf("failed to retrieve object kind for %s: %w", objName, err)
	}
	obj.GetObjectKind().SetGroupVersionKind(objKind)

	uObject, err := runtime.DefaultUnstructuredConverter.ToUnstructured(obj)
	if err != nil {
		return "", fmt.Errorf(
			"failed to convert %s %s to unstructured: %w",
			obj.GetObjectKind().GroupVersionKind().Kind,
			client.ObjectKeyFromObject(obj), err)
	}
	u := &unstructured.Unstructured{Object: uObject}
	rootNode := unstructuredNode{Unstructured: u}
	rootNode.Status, err = kstatus.Compute(u)
	if err != nil {
		log.Printf("Failed to compute status of %s %s: %s", objKind.Kind, objName, err)
	}
	tree := tree.New(&rootNode)

	if err := getFluxInventory(ctx, u, tree, clientConfig); err != nil {
		return "", err
	}

	if opts.SkipReadyResources {
		markResources(tree)
		tree = filterReadyResources(tree)
	}

	return tree.Print(), nil
}

// Mark resource to display (the ones that are not Ready or have non-Ready descendants)
func markResources(tree tree.Tree) bool {
	node := tree.Node().(*unstructuredNode)
	childNotReady := false
	for _, res := range tree.Items() {
		if markResources(res) {
			childNotReady = true
		}
	}
	node.Show = childNotReady || node.Status.Status != kstatus.CurrentStatus
	return node.Show
}

// Return a sub tree containing only nodes that have to be displayed
func filterReadyResources(fullTree tree.Tree) tree.Tree {
	filteredTree := tree.New(fullTree.Node())
	for _, childTree := range fullTree.Items() {
		childNode := childTree.Node().(*unstructuredNode)
		if childNode.Show {
			filteredTree.AddTree(filterReadyResources(childTree))
		}
	}
	return filteredTree
}

const header string = "IDENTIFIER\tSTATUS\tREASON\tMESSAGE\t"

type statusLine struct {
	identifier string
	status     string
	reason     string
	message    string
}

func (res statusLine) string() string {
	return fmt.Sprintf("%s\t%s\t%s\t%s\t", res.identifier, res.status, res.reason, res.message)
}

// Unstuctured printer
type unstructuredNode struct {
	*unstructured.Unstructured
	Owned  []*unstructuredNode
	Events []*eventv1.Event
	Status *kstatus.Result
	Show   bool
}

func (res unstructuredNode) Text(tree tree.Tree) string {
	out := statusLine{}
	if res.GetNamespace() == "" {
		out.identifier = fmt.Sprintf("%s/%s", res.GetKind(), res.GetName())
	} else {
		out.identifier = fmt.Sprintf("%s/%s/%s", res.GetKind(), res.GetNamespace(), res.GetName())
	}

	if res.Status == nil {
		status, err := kstatus.Compute(res.Unstructured)
		if err != nil {
			out.message = fmt.Sprintf("Failed to compute object %s status: %s", client.ObjectKeyFromObject(res.Unstructured), err)
			return out.string()
		}
		res.Status = status
	}
	if res.Status.Status == kstatus.CurrentStatus {
		// Change kstatus "Current" status to "Ready" as it is more self-explanatory
		out.status = "Ready"
	} else {
		out.status = res.Status.Status.String()
	}
	out.message = res.Status.Message

	// If resource is a leaf and is not ready, print all its conditions and events
	if res.Status.Status != kstatus.CurrentStatus && len(tree.Items()) == 0 {
		if objWithConditions, err := kstatus.GetObjectWithConditions(res.Object); err == nil && len(objWithConditions.Status.Conditions) > 0 {
			condNode := tree.Add(headerNode{"Conditions"})
			for _, cond := range objWithConditions.Status.Conditions {
				condNode.Add(conditionNode{cond})
			}
		}
		if len(res.Events) > 0 {
			eventsNode := tree.Add(headerNode{"Events"})

			sortedEvents := TimeSortableEvents(res.Events)
			sort.Sort(sortedEvents)

			for _, event := range sortedEvents {
				eventsNode.Add(eventNode{event})
			}
		}
	}
	return out.string()
}

// BasicCondition printer
type conditionNode struct {
	kstatus.BasicCondition
}

func (cond conditionNode) Text(tree tree.Tree) string {
	return statusLine{
		identifier: string(cond.Type),
		status:     string(cond.Status),
		reason:     cond.Reason,
		message:    cond.Message,
	}.string()
}

// Event printer
type eventNode struct {
	*eventv1.Event
}

const EventTimeFormat = "2006-01-02 15:04:05"

/*
getFirstAndLastTimes extracts relevant timestamps and count information from an event

checks the reference fields of the newer events.events.k8s.io API (eventv1.Event)
and if unavailable, it falls back to the DeprecatedFoo fields for compatibility with the older v1/Event API
If necessary, it also uses the resource's creationTimestamps as a fallback
*/
func getFirstAndLastTimes(event eventv1.Event) (firstTime, lastTime time.Time, count int32) {
	if !event.EventTime.IsZero() {
		firstTime = event.EventTime.Time
	} else if !event.DeprecatedFirstTimestamp.IsZero() {
		firstTime = event.DeprecatedFirstTimestamp.Time
	} else {
		firstTime = event.CreationTimestamp.Time
	}

	if event.Series != nil {
		lastTime = event.Series.LastObservedTime.Time
		count = event.Series.Count
	} else if !event.DeprecatedLastTimestamp.IsZero() {
		lastTime = event.DeprecatedLastTimestamp.Time
		count = event.DeprecatedCount
	} else {
		lastTime = firstTime
		count = 1
	}

	return
}

func (event eventNode) Text(tree tree.Tree) string {

	firstEventTime, lastEventTime, count := getFirstAndLastTimes(*event.Event)

	var displayEventTime string
	if count > 1 {
		displayEventTime = fmt.Sprintf("%s (x%d over %s)",
			lastEventTime.UTC().Format(EventTimeFormat),
			count,
			lastEventTime.Sub(firstEventTime))
	} else {
		displayEventTime = lastEventTime.UTC().Format(EventTimeFormat)
	}

	return statusLine{
		identifier: displayEventTime,
		status:     event.Type,
		reason:     event.Reason,
		message:    event.Note,
	}.string()
}

// eventNode comparison for sorting

type TimeSortableEvents []*eventv1.Event

func (events TimeSortableEvents) Len() int      { return len(events) }
func (events TimeSortableEvents) Swap(a, b int) { events[a], events[b] = events[b], events[a] }
func (events TimeSortableEvents) Less(a, b int) bool {
	// we want to have chronological ordering (more recent last)
	// so an event is "less" (ie. up the list) if it is less recent,
	// ie. "less" is "before"
	// ie. "less(i,j)" is "i.before(j)"

	firstEventTimeA, lastEventTimeA, countA := getFirstAndLastTimes(*events[a])
	firstEventTimeB, lastEventTimeB, countB := getFirstAndLastTimes(*events[b])

	// we compare by last event time first, then by first event time, then count as tie breaker

	if lastEventTimeB.Equal(lastEventTimeA) {
		if firstEventTimeB.Equal(firstEventTimeA) {
			return countA > countB
		}
		return firstEventTimeA.Before(firstEventTimeB)
	}
	return lastEventTimeA.Before(lastEventTimeB)
}

// Text printer
type headerNode struct {
	text string
}

func (node headerNode) Text(tree tree.Tree) string {
	return statusLine{identifier: fmt.Sprintf("┬┄┄[%s]", node.text)}.string()
}
