/*
Copyright 2023 The Sylva authors


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

derived from github.com/d6o/GoTree
Copyright (c) 2013-2018 Diego Siqueira
*/

// Package gotree create and print tree.
package tree

import (
	"strings"
)

const (
	newLine      = "\n"
	emptySpace   = "   "
	middleItem   = "├┄╴"
	continueItem = "┆  "
	lastItem     = "╰┄╴"
)

type (
	tree struct {
		node  Node
		items []Tree
	}

	Node interface {
		Text(tree Tree) string
	}

	// Tree is tree interface
	Tree interface {
		Add(node Node) Tree
		AddTree(tree Tree)
		Node() Node
		Items() []Tree
		Text() string
		Print() string
	}

	printer struct {
	}

	// Printer is printer interface
	Printer interface {
		Print(Tree) string
	}
)

// New returns a new tree.Tree
func New(node Node) Tree {
	return &tree{
		node:  node,
		items: []Tree{},
	}
}

// Add adds a node to the tree
func (t *tree) Add(node Node) Tree {
	n := New(node)
	t.items = append(t.items, n)
	return n
}

// AddTree adds a tree as an item
func (t *tree) AddTree(tree Tree) {
	t.items = append(t.items, tree)
}

// Node returns the node object
func (t *tree) Node() Node {
	return t.node
}

// Text returns the node's value
func (t *tree) Text() string {
	return t.node.Text(t)
}

// Items returns all items in the tree
func (t *tree) Items() []Tree {
	return t.items
}

// Print returns an visual representation of the tree
func (t *tree) Print() string {
	return newPrinter().Print(t)
}

func newPrinter() Printer {
	return &printer{}
}

// Print prints a tree to a string
func (p *printer) Print(t Tree) string {
	return t.Text() + newLine + p.printItems(t.Items(), []bool{})
}

func (p *printer) printText(text string, spaces []bool, last bool) string {
	var result string
	for _, space := range spaces {
		if space {
			result += emptySpace
		} else {
			result += continueItem
		}
	}

	indicator := middleItem
	if last {
		indicator = lastItem
	}

	var out string
	lines := strings.Split(text, "\n")
	for i := range lines {
		text := lines[i]
		if i == 0 {
			out += result + indicator + text + newLine
			continue
		}
		if last {
			indicator = emptySpace
		} else {
			indicator = continueItem
		}
		out += result + indicator + text + newLine
	}

	return out
}

func (p *printer) printItems(t []Tree, spaces []bool) string {
	var result string
	for i, f := range t {
		last := i == len(t)-1
		result += p.printText(f.Text(), spaces, last)
		if len(f.Items()) > 0 {
			spacesChild := append(spaces, last)
			result += p.printItems(f.Items(), spacesChild)
		}
	}
	return result
}
