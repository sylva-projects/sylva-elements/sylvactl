/*
Copyright 2023 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package informer

import (
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	testenv "gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/test-env"
)

var _ = Describe("sylvactl dependencies tests", func() {

	Context("When creating interdependent resources", func() {

		It("Handles resources dependencies as expected", func() {

			By("Start the playback of test scenario")
			player, err := testenv.NewRecordPlayer(ctx, env.Client(), testenv.FilePath("scenarios/dependencies.yaml"))
			Expect(err).NotTo(HaveOccurred())
			Expect(player).NotTo(BeNil())

			err = player.WaitForCacheSync()
			Expect(err).NotTo(HaveOccurred())

			By("Start the informer")

			awaitedObj := "Kustomization/sylva-system/indirect-dependent"
			objUpdate := make(chan ObjEvent, 1000)
			informer, err := NewInformer(ctx, env.ClientConfig(), objUpdate,
				awaitedObj, false, false, 10*time.Millisecond, "sylva-system", "")
			Expect(err).NotTo(HaveOccurred())

			By("Ensure that all objects are seen as ready")
			indirectDependent, found := informer.ObjectMap.Get(awaitedObj)
			Expect(found).To(BeTrue())
			Expect(indirectDependent.Reconciled()).To(BeTrue())

			By("Resume playback")

			// notify player that informer has synced
			player.Resume()

			By("Ensure that dependents statuses have been updated")

			for {
				select {
				case <-ctx.Done():
					Fail(fmt.Sprintf("Context canceled, test did not finish within %d seconds", int(timeout.Seconds())))
				case err := <-player.Error():
					Expect(err).NotTo(HaveOccurred())
				case event := <-objUpdate:
					objName := event.FluxObject.QualifiedName()

					fmt.Printf("Processing event for object: %s\n", objName)

					if objName == "Kustomization/sylva-system/dependency" {
						Expect(event.Ready()).To(BeFalse())
						Expect(indirectDependent.Reconciled()).To(BeFalse())
						return
					}
				}
			}
		})
	})
})

var _ = Describe("sylvactl oci tests", func() {

	Context("When creating helmRelease with chartRef", func() {

		It("Handles resources dependencies as expected", func() {

			By("Start the playback of test scenario")

			player, err := testenv.NewRecordPlayer(ctx, env.Client(), testenv.FilePath("scenarios/helmrelease-chartref.yaml"))
			Expect(err).NotTo(HaveOccurred())
			Expect(player).NotTo(BeNil())

			By("Start the informer")

			awaitedObj := "HelmRelease/sylva-units-preview/sylva-units"
			objUpdate := make(chan ObjEvent, 1000)
			informer, err := NewInformer(ctx, env.ClientConfig(), objUpdate,
				awaitedObj, false, false, 10*time.Millisecond, "sylva-units-preview", "")
			Expect(err).NotTo(HaveOccurred())

		loop:
			for {
				select {
				case <-ctx.Done():
					return
				case err := <-player.Error():
					Expect(err).NotTo(HaveOccurred())
				case event := <-objUpdate:

					objName := event.FluxObject.QualifiedName()
					fmt.Printf("Processing event for object: %s\n", objName)

					if objName == awaitedObj && event.FluxObject.Ready() {
						break loop
					}
				}
			}
			sylvaUnits, found := informer.ObjectMap.Get("HelmRelease/sylva-units-preview/sylva-units")
			Expect(found).To(BeTrue())
			Expect(len(sylvaUnits.ListDependencies())).To(Equal(1))
		})
	})
})
