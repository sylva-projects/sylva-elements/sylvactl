/*
Copyright 2023 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package informer

import (
	"fmt"
	"os"
	"reflect"
	"sort"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"sigs.k8s.io/yaml"

	testenv "gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/test-env"
)

const recordName = "bootstrap-mgt-capd"

var _ = Describe("sylvactl replay tests", func() {

	Context("When replaying capd bootstrap record", func() {

		It("Handles resources statuses as expected", func() {

			By("Start the playback of test scenario")

			player, err := testenv.NewRecordPlayer(ctx, env.Client(), testenv.FilePath(fmt.Sprintf("scenarios/%s.yaml", recordName)))
			Expect(err).NotTo(HaveOccurred())
			Expect(player).NotTo(BeNil())

			err = player.WaitForCacheSync()
			Expect(err).NotTo(HaveOccurred())

			awaitedObj := "Kustomization/sylva-system/management-sylva-units"
			objUpdate := make(chan ObjEvent, 1000)
			informer, err := NewInformer(ctx, env.ClientConfig(), objUpdate,
				awaitedObj, false, false, 10*time.Millisecond, "sylva-system", "")
			Expect(err).NotTo(HaveOccurred())

			// notify player that informer has synced
			player.Resume()

			By("Analyse events sent by informer")

			observedSeq := statusSequence{}

			for {
				select {
				case <-ctx.Done():
					Fail(fmt.Sprintf("Context canceled, test did not finish within %d seconds", int(timeout.Seconds())))
				case err := <-player.Error():
					Expect(err).NotTo(HaveOccurred())
				case event := <-objUpdate:
					objName := event.FluxObject.QualifiedName()
					informer.ObjectMap.FilterDependencies(awaitedObj)

					objEventStatus := objEventStatus{
						EventObject:     objName,
						DeleteEvent:     event.Delete,
						ObjectStatusMap: map[string]objStatus{},
					}
					for _, obj := range informer.ObjectMap.Items() {
						objEventStatus.ObjectStatusMap[obj.QualifiedName()] = getObjStatus(obj, informer)
					}
					observedSeq = append(observedSeq, objEventStatus)

					if event.Delete {
						fmt.Printf("%s has been deleted\n", objName)
						continue
					}
					if event.FluxObject.Ready() {
						fmt.Printf("%s is ready\n", objName)
					}

					if objName == "Kustomization/sylva-system/cluster" && event.FluxObject.Ready() {

						clusterHR, found := informer.ObjectMap.Get("HelmRelease/sylva-system/cluster")
						Expect(found).To(BeTrue())
						Expect(clusterHR.Ready()).To(BeTrue())
						Expect(clusterHR.Watched()).To(BeTrue())

						clusterReady, found := informer.ObjectMap.Get("Kustomization/sylva-system/cluster-ready")
						Expect(found).To(BeTrue())
						Expect(clusterReady.Ready()).To(BeFalse())
						Expect(informer.Reconcilable(clusterReady)).To(BeTrue())

						suStatus, found := informer.ObjectMap.Get("Kustomization/sylva-system/sylva-units-status")
						Expect(found).To(BeTrue())
						Expect(suStatus.Ready()).To(BeFalse())
						Expect(informer.Reconcilable(suStatus)).To(BeFalse())

						pivot, found := informer.ObjectMap.Get("Kustomization/sylva-system/pivot")
						Expect(found).To(BeTrue())
						Expect(pivot.Ready()).To(BeFalse())
						Expect(pivot.Watched()).To(BeFalse())
						Expect(informer.Reconcilable(pivot)).To(BeFalse())
					}

					if objName == awaitedObj && event.FluxObject.Ready() {
						// Ensure that all watched objects are ready
						for _, obj := range informer.ObjectMap.Items() {
							if obj.Watched() {
								Expect(obj.Ready()).To(BeTrue())
							}
						}
						fmt.Printf("Awaited object %s is ready\n", awaitedObj)

						err = saveEventSequence(observedSeq)
						Expect(err).NotTo(HaveOccurred())
						err = compareEventSequence(observedSeq)
						Expect(err).NotTo(HaveOccurred())

						return
					}
				}
			}
		})
	})
})

func saveEventSequence(seq statusSequence) error {
	yamlSeq, err := yaml.Marshal(seq)
	if err != nil {
		return err
	}
	return os.WriteFile(testenv.FilePath(fmt.Sprintf("scenarios/%s-observedEventSequence.yaml", recordName)), yamlSeq, 0644) //nolint:gosec
}

func compareEventSequence(seq statusSequence) error {

	data, err := os.ReadFile(testenv.FilePath(fmt.Sprintf("scenarios/%s-referenceEventSequence.yaml", recordName)))
	if err != nil {
		return err
	}
	refSequence := statusSequence{}
	err = yaml.Unmarshal(data, &refSequence)
	if err != nil {
		return err
	}
	Expect(reflect.DeepEqual(seq, refSequence)).To(BeTrue())
	return nil
}

func getObjStatus(obj Node, informer *TreeInformer) objStatus {

	objDeps := []string{}
	for _, dep := range informer.ObjectMap.GetDependencies(obj.QualifiedName()) {
		objDeps = append(objDeps, dep.QualifiedName())
	}
	sort.Slice(objDeps, func(i, j int) bool {
		return objDeps[i] < objDeps[j]
	})

	return objStatus{
		Ready:        obj.Ready(),
		Reconciled:   obj.Reconciled(),
		Reconcilable: informer.Reconcilable(obj),
		Watched:      obj.Watched(),
		Status:       obj.Status(),
		Dependencies: objDeps,
	}
}

type objStatus struct {
	Ready        bool     `json:"ready"`
	Reconciled   bool     `json:"reconciled"`
	Reconcilable bool     `json:"reconcilable"`
	Watched      bool     `json:"watched"`
	Status       string   `json:"status"`
	Dependencies []string `json:"dependencies"`
}

type objEventStatus struct {
	EventObject     string               `json:"eventObject"`
	DeleteEvent     bool                 ` json:"deleteEvent"`
	ObjectStatusMap map[string]objStatus `json:"objectStatusMap"`
}
type statusSequence []objEventStatus
