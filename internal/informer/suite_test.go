/*
Copyright 2023 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package informer

import (
	"context"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	testenv "gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/test-env"
)

var env *testenv.TestEnv
var ctx context.Context
var cancel context.CancelFunc

const timeout time.Duration = 60 * time.Second

func TestInformer(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Sylvactl informer tests")
}

var _ = BeforeEach(func() {
	log.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	ctx, cancel = context.WithTimeout(context.Background(), timeout)
	var err error
	env, err = testenv.NewEnv()
	Expect(err).NotTo(HaveOccurred())
	Expect(env).NotTo(BeNil())
})

var _ = AfterEach(func() {
	By("tearing down the test environment")
	cancel()
	err := env.Stop()
	Expect(err).NotTo(HaveOccurred())
})
