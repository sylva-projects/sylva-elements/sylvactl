package informer

import (
	"fmt"
	"strings"

	"sigs.k8s.io/controller-runtime/pkg/client"

	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/cli-utils/pkg/object"

	helmv2 "github.com/fluxcd/helm-controller/api/v2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"
	"github.com/fluxcd/pkg/apis/meta"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	sourcev1beta2 "github.com/fluxcd/source-controller/api/v1beta2"
)

const fmtSeparator = "/"

type FluxObject interface {
	FluxAdapter
	QualifiedName() string
	DisplayName() string
	computeStatus(FluxObject)
	GetAnnotationValue(annotationName string) string
	HasHandledReconcileAnnotation() bool
	HasMissingDependencies() bool
	GetMissingDependencies() []string
	SetMissingDependencies([]string)
	Status() string
	Ready() bool
	Stalled() bool
}

type fluxObject struct {
	FluxAdapter
	ready               bool
	missingDependencies []string
	status              string
	defaultNamespace    string
	qualifiedName       string
}

func NewFluxObject(obj FluxAdapter, defaultNs string) FluxObject {
	return &fluxObject{
		FluxAdapter:      obj,
		defaultNamespace: defaultNs,
		qualifiedName:    qualifiedName(obj.GetObjectKind().GroupVersionKind().Kind, obj.GetNamespace(), obj.GetName()),
	}
}

func (object *fluxObject) Ready() bool {
	return object.ready
}

func (object *fluxObject) Stalled() bool {
	stalledCond := apimeta.FindStatusCondition(object.GetConditions(), meta.StalledCondition)
	if stalledCond != nil && stalledCond.Status == metav1.ConditionTrue {
		return true
	} else {
		return false
	}
}

func (object *fluxObject) HasMissingDependencies() bool {
	return len(object.missingDependencies) > 0
}

func (object *fluxObject) GetMissingDependencies() []string {
	return object.missingDependencies
}

func (object *fluxObject) SetMissingDependencies(missing []string) {
	if (len(object.missingDependencies) == 0) != (len(missing) == 0) {
		// HasMissingDependencies result will change with that update of missingDependencies list
		// so we have to reprocess object status as it will be impacted.
		defer object.computeStatus(object)
	}
	object.missingDependencies = missing
}

func (object *fluxObject) Status() string {
	return object.status
}

func (object *fluxObject) GetAnnotationValue(annotationName string) string {
	if ann := object.GetAnnotations(); ann != nil {
		if annotationValue, ok := ann[annotationName]; ok {
			return annotationValue
		}
	}
	return ""
}

func (object *fluxObject) HasHandledReconcileAnnotation() bool {
	reconcileRequestedAt := object.GetAnnotationValue(meta.ReconcileRequestAnnotation)
	return reconcileRequestedAt == "" || object.GetLastHandledReconcileAt() == reconcileRequestedAt
}

// Compute readiness and message for the object, based on Ready condition if any.
// preserve previous state during periodic Kustomizations & HelmReleases reconciliations
func (object *fluxObject) computeStatus(prevObject FluxObject) {
	readyCond := apimeta.FindStatusCondition(object.GetConditions(), meta.ReadyCondition)

	if object.HasMissingDependencies() {
		object.status = fmt.Sprintf("Not Ready - Missing dependencies: %s", strings.Join(object.missingDependencies, ", "))
		object.ready = false

	} else if object.IsStatic() {
		object.status = "Static object - Ready"
		object.ready = true
	} else if object.Suspended() {
		object.status = "Suspended"
		object.ready = false
	} else if object.Stalled() {
		stalledCond := apimeta.FindStatusCondition(object.GetConditions(), meta.StalledCondition)
		object.status = fmt.Sprintf("Stalled - %s: %s", stalledCond.Reason, stalledCond.Message)
		object.ready = false

	} else if object.GetObservedGeneration() != object.GetGeneration() {
		if readyCond != nil && readyCond.Status != metav1.ConditionTrue {
			object.status = fmt.Sprintf("%s - %s", readyCond.Reason, readyCond.Message)
		} else {
			object.status = fmt.Sprintf("Updated - ObservedGeneration is %d whereas Generation is %d",
				object.GetObservedGeneration(), object.GetGeneration())
		}
		object.ready = false

	} else if !object.HasHandledReconcileAnnotation() {
		if readyCond != nil && readyCond.Status != metav1.ConditionTrue {
			object.status = fmt.Sprintf("%s - %s", readyCond.Reason, readyCond.Message)
		} else {
			object.status = fmt.Sprintf("Updated - lastHandledReconcileAt is %s whereas reconcileRequestedAt is %s",
				object.GetLastHandledReconcileAt(), object.GetAnnotationValue(meta.ReconcileRequestAnnotation))
		}
		object.ready = false

	} else if readyCond != nil && readyCond.ObservedGeneration != object.GetGeneration() {
		object.status = fmt.Sprintf("Unexpected state: %s/%s - Ready.ObservedGeneration is %d whereas Generation is %d", readyCond.Reason, readyCond.Message, readyCond.ObservedGeneration, object.GetGeneration())
		object.ready = false

	} else if readyCond != nil && (readyCond.Reason == meta.ProgressingReason || readyCond.Reason == meta.DependencyNotReadyReason) && prevObject != nil && prevObject.Ready() {
		// Preserve unit status during periodic reconcile/healthChecks
		object.status = prevObject.Status()
		object.ready = prevObject.Ready()

	} else if readyCond != nil {
		// Finally trust readyCondition if any
		object.status = fmt.Sprintf("%s - %s", readyCond.Reason, readyCond.Message)
		object.ready = readyCond.Status == metav1.ConditionTrue

	} else {
		object.status = "Unknown - Failed to compute object status"
		object.ready = false
	}
}

func (object *fluxObject) DisplayName() string {
	if object.defaultNamespace == object.GetNamespace() {
		return strings.Join([]string{object.GetObjectKind().GroupVersionKind().Kind, object.GetName()}, fmtSeparator)
	} else {
		return object.qualifiedName
	}
}

func (object *fluxObject) QualifiedName() string {
	return object.qualifiedName
}

type FluxAdapter interface {
	objectWithConditions
	DeepCopy() FluxAdapter
	ClientObject() client.Object
	Suspended() bool
	GetLastHandledReconcileAt() string
	IsStatic() bool
	GetObservedGeneration() int64
	ListDependencies() []string
	setSuspend(bool)
}

type objectWithConditions interface {
	client.Object
	GetConditions() []metav1.Condition
}

func qualifiedName(kind string, namespace string, name string) string {
	return strings.Join([]string{kind, namespace, name}, fmtSeparator)
}

// kustomizev1.Kustomization

type kustomizationAdapter struct {
	*kustomizev1.Kustomization
}

func (obj kustomizationAdapter) DeepCopy() FluxAdapter {
	return kustomizationAdapter{obj.Kustomization.DeepCopy()}
}

func (obj kustomizationAdapter) ClientObject() client.Object {
	return obj.Kustomization
}

func (obj kustomizationAdapter) Suspended() bool {
	return obj.Kustomization.Spec.Suspend
}

func (obj kustomizationAdapter) setSuspend(suspended bool) {
	obj.Kustomization.Spec.Suspend = suspended
}

func (obj kustomizationAdapter) GetObservedGeneration() int64 {
	return obj.Kustomization.Status.ObservedGeneration
}

func (obj kustomizationAdapter) GetLastHandledReconcileAt() string {
	return obj.Kustomization.Status.LastHandledReconcileAt
}

func (obj kustomizationAdapter) IsStatic() bool {
	return false
}

func (obj kustomizationAdapter) ListDependencies() []string {
	deps := []string{}
	for _, dep := range obj.Kustomization.Spec.DependsOn {
		ns := dep.Namespace
		if ns == "" {
			ns = obj.Kustomization.Namespace
		}
		deps = append(deps, qualifiedName(kustomizev1.KustomizationKind, ns, dep.Name))
	}
	src := obj.Kustomization.Spec.SourceRef
	if src.Namespace == "" {
		src.Namespace = obj.Kustomization.Namespace
	}
	deps = append(deps, src.String())
	return deps
}

// Special function to retrieve HelmReleases produced by a Kustomization
func GetChildHelmReleases(obj FluxObject) (hrList []string) {
	if ks, ok := obj.ClientObject().(*kustomizev1.Kustomization); ok {
		if ks.Status.Inventory != nil {
			for _, entry := range ks.Status.Inventory.Entries {
				if objMetadata, err := object.ParseObjMetadata(entry.ID); err == nil {
					if objMetadata.GroupKind.Group == helmv2.GroupVersion.Group &&
						objMetadata.GroupKind.Kind == helmv2.HelmReleaseKind {
						objName := qualifiedName(helmv2.HelmReleaseKind, objMetadata.Namespace, objMetadata.Name)
						hrList = append(hrList, objName)
					}
				}
			}
		}
	}
	return hrList
}

// helmv2.HelmRelease

type helmReleaseAdapter struct {
	*helmv2.HelmRelease
}

func (obj helmReleaseAdapter) DeepCopy() FluxAdapter {
	return helmReleaseAdapter{obj.HelmRelease.DeepCopy()}
}

func (obj helmReleaseAdapter) ClientObject() client.Object {
	return obj.HelmRelease
}

func (obj helmReleaseAdapter) Suspended() bool {
	return obj.HelmRelease.Spec.Suspend
}

func (obj helmReleaseAdapter) setSuspend(suspended bool) {
	obj.HelmRelease.Spec.Suspend = suspended
}

func (obj helmReleaseAdapter) GetObservedGeneration() int64 {
	return obj.HelmRelease.Status.ObservedGeneration
}

func (obj helmReleaseAdapter) GetLastHandledReconcileAt() string {
	return obj.HelmRelease.Status.LastHandledReconcileAt
}

func (obj helmReleaseAdapter) IsStatic() bool {
	return false
}

func (obj helmReleaseAdapter) ListDependencies() []string {
	deps := []string{}
	for _, dep := range obj.HelmRelease.Spec.DependsOn {
		ns := dep.Namespace
		if ns == "" {
			ns = obj.HelmRelease.Namespace
		}
		deps = append(deps, qualifiedName(helmv2.HelmReleaseKind, ns, dep.Name))
	}
	if obj.HelmRelease.Spec.Chart != nil {
		chartName := qualifiedName(sourcev1.HelmChartKind,
			obj.HelmRelease.Spec.Chart.GetNamespace(obj.HelmRelease.Namespace), obj.HelmRelease.GetHelmChartName())
		deps = append(deps, chartName)
	}
	if obj.HelmRelease.Spec.ChartRef != nil {
		chartNamespace := obj.HelmRelease.Spec.ChartRef.Namespace
		if chartNamespace == "" {
			chartNamespace = obj.HelmRelease.Namespace
		}
		chartName := qualifiedName(obj.HelmRelease.Spec.ChartRef.Kind, chartNamespace, obj.HelmRelease.Spec.ChartRef.Name)
		deps = append(deps, chartName)
	}
	return deps
}

// sourcev1.HelmChart

type helmChartAdapter struct {
	*sourcev1.HelmChart
}

func (obj helmChartAdapter) DeepCopy() FluxAdapter {
	return helmChartAdapter{obj.HelmChart.DeepCopy()}
}

func (obj helmChartAdapter) ClientObject() client.Object {
	return obj.HelmChart
}

func (obj helmChartAdapter) Suspended() bool {
	return obj.HelmChart.Spec.Suspend
}

func (obj helmChartAdapter) setSuspend(suspended bool) {
	obj.HelmChart.Spec.Suspend = suspended
}

func (obj helmChartAdapter) GetObservedGeneration() int64 {
	return obj.HelmChart.Status.ObservedGeneration
}

func (obj helmChartAdapter) GetLastHandledReconcileAt() string {
	return obj.HelmChart.Status.LastHandledReconcileAt
}

func (obj helmChartAdapter) IsStatic() bool {
	return false
}

func (obj helmChartAdapter) ListDependencies() []string {
	return []string{qualifiedName(obj.HelmChart.Spec.SourceRef.Kind, obj.HelmChart.Namespace, obj.HelmChart.Spec.SourceRef.Name)}
}

// sourcev1.HelmRepository

type helmRepositoryAdapter struct {
	*sourcev1.HelmRepository
}

func (obj helmRepositoryAdapter) DeepCopy() FluxAdapter {
	return helmRepositoryAdapter{obj.HelmRepository.DeepCopy()}
}

func (obj helmRepositoryAdapter) ClientObject() client.Object {
	return obj.HelmRepository
}

func (obj helmRepositoryAdapter) Suspended() bool {
	return obj.HelmRepository.Spec.Suspend
}

func (obj helmRepositoryAdapter) setSuspend(suspended bool) {
	obj.HelmRepository.Spec.Suspend = suspended
}

func (obj helmRepositoryAdapter) GetObservedGeneration() int64 {
	if obj.HelmRepository.Spec.Type == sourcev1.HelmRepositoryTypeOCI {
		// this CR is "static" when type is OCI (see https://github.com/fluxcd/source-controller/pull/1243)
		return obj.HelmRepository.ObjectMeta.Generation
	} else {
		return obj.HelmRepository.Status.ObservedGeneration
	}
}

func (obj helmRepositoryAdapter) GetLastHandledReconcileAt() string {
	return obj.HelmRepository.Status.LastHandledReconcileAt
}

func (obj helmRepositoryAdapter) IsStatic() bool {
	if obj.HelmRepository.Spec.Type == sourcev1.HelmRepositoryTypeOCI {
		// this CR is "static" when type is OCI (see https://github.com/fluxcd/source-controller/pull/1243)
		return true
	} else {
		return false
	}
}

func (obj helmRepositoryAdapter) ListDependencies() []string {
	return []string{}
}

// sourcev1.GitRepository

type gitRepositoryAdapter struct {
	*sourcev1.GitRepository
}

func (obj gitRepositoryAdapter) DeepCopy() FluxAdapter {
	// var foo FluxAdapter = gitRepositoryAdapter{obj.GitRepository.DeepCopy()}
	// return foo
	return gitRepositoryAdapter{obj.GitRepository.DeepCopy()}
}

func (obj gitRepositoryAdapter) ClientObject() client.Object {
	return obj.GitRepository
}

func (obj gitRepositoryAdapter) Suspended() bool {
	return obj.GitRepository.Spec.Suspend
}

func (obj gitRepositoryAdapter) setSuspend(suspended bool) {
	obj.GitRepository.Spec.Suspend = suspended
}

func (obj gitRepositoryAdapter) GetObservedGeneration() int64 {
	return obj.GitRepository.Status.ObservedGeneration
}

func (obj gitRepositoryAdapter) GetLastHandledReconcileAt() string {
	return obj.GitRepository.Status.LastHandledReconcileAt
}

func (obj gitRepositoryAdapter) IsStatic() bool {
	return false
}

func (obj gitRepositoryAdapter) ListDependencies() []string {
	return []string{}
}

// sourcev1beta2.OCIRepository

type ociRepositoryAdapter struct {
	*sourcev1beta2.OCIRepository
}

func (obj ociRepositoryAdapter) DeepCopy() FluxAdapter {
	return ociRepositoryAdapter{obj.OCIRepository.DeepCopy()}
}

func (obj ociRepositoryAdapter) ClientObject() client.Object {
	return obj.OCIRepository
}

func (obj ociRepositoryAdapter) Suspended() bool {
	return obj.OCIRepository.Spec.Suspend
}

func (obj ociRepositoryAdapter) setSuspend(suspended bool) {
	obj.OCIRepository.Spec.Suspend = suspended
}

func (obj ociRepositoryAdapter) GetObservedGeneration() int64 {
	return obj.OCIRepository.Status.ObservedGeneration
}

func (obj ociRepositoryAdapter) GetLastHandledReconcileAt() string {
	return obj.OCIRepository.Status.LastHandledReconcileAt
}

func (obj ociRepositoryAdapter) IsStatic() bool {
	return false
}

func (obj ociRepositoryAdapter) ListDependencies() []string {
	return []string{}
}
