/*
Copyright 2022 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package informer

import (
	"errors"
	"fmt"
	"log"
	"reflect"
	"sort"
	"strings"
	"sync"

	"github.com/heimdalr/dag"
)

type (
	// Use dag edge a -> b to represent the fact that b depends on a.
	// roots/parents will be dependencies, leafs/childrens will be dependents
	objectMap struct {
		dag *dag.DAG
	}

	ObjectMap interface {
		Store(object FluxObject, updateDeps bool) (FluxObject, Node, error)
		Remove(object FluxObject) error
		Items() NodeList
		Get(qualifiedName string) (Node, bool)
		UpdateAllDependencies() error
		UpdateDependencies(Node) error
		SortDependencies(source NodeList) (result NodeList)
		FilterDependencies(nodeName string) NodeList
		DependsOn(dependent string, dependency string) bool
		GetDependencies(nodeName string) NodeList
		GetDependents(nodeName string) NodeList
		ProcessReconciledStatus(nodeName string)
		OrderedDependencies() []Node
	}

	node struct {
		FluxObject
		sync.RWMutex
		reconciled bool
		watched    bool
	}

	Node interface {
		FluxObject
		GetObject() FluxObject
		setObject(FluxObject)
		Reconciled() bool
		setReconciled(bool)
		Watched() bool
		setWatched(bool)
	}

	NodeList []Node
)

// objectMap
func NewMap() ObjectMap {
	return &objectMap{
		dag: dag.NewDAG(),
	}
}

func MapFrom(dag *dag.DAG) ObjectMap {
	return &objectMap{
		dag: dag,
	}
}

// Stores object in map, update dependencies, return node and previous Object if there was any
func (m *objectMap) Store(obj FluxObject, updateDeps bool) (FluxObject, Node, error) {
	var oldObject FluxObject
	var err error
	node, _ := m.dag.GetVertex(obj.QualifiedName())
	if node == nil {
		node = NewNode(obj)
		if err = m.dag.AddVertexByID(obj.QualifiedName(), node); err != nil {
			return nil, nil, err
		}
		err = errors.Join(err, m.UpdateAllDependencies())
	} else {
		oldObject = node.(Node).GetObject()
		node.(Node).setObject(obj)
	}
	if oldObject == nil || !reflect.DeepEqual(oldObject.ListDependencies(), node.(Node).ListDependencies()) {
		err = errors.Join(err, m.UpdateDependencies(node.(Node)))
	} else {
		node.(Node).SetMissingDependencies(oldObject.GetMissingDependencies())
	}
	node.(Node).computeStatus(oldObject)
	return oldObject, node.(Node), err
}

func (m *objectMap) Remove(obj FluxObject) (removeError error) {
	childrens, err := m.dag.GetChildren(obj.QualifiedName())
	if err != nil {
		return err
	}
	for _, dependent := range childrens {
		defer func() {
			if err := m.UpdateDependencies(dependent.(Node)); err != nil {
				removeError = errors.Join(removeError, err)
			}
		}()
	}
	if err := m.dag.DeleteVertex(obj.QualifiedName()); err != nil {
		return // error means that object is not in dag
	}
	// Update missing dependencies as we may also have broken a loop
	if err := m.UpdateAllDependencies(); err != nil {
		removeError = errors.Join(removeError, err)
	}
	return removeError
}

func (m *objectMap) Get(qualifiedName string) (Node, bool) {
	node, _ := m.dag.GetVertex(qualifiedName)
	if node != nil {
		return node.(Node), true
	} else {
		return nil, false
	}
}

func (m *objectMap) Items() NodeList {
	// FIXME: there should be a better way to do that...
	res := NodeList{}
	for _, obj := range m.dag.GetVertices() {
		res = append(res, obj.(Node))
	}
	return res
}

// Returns direct dependents of a node
func (m *objectMap) GetDependents(nodeID string) NodeList {
	res := NodeList{}
	if childrens, err := m.dag.GetChildren(nodeID); err == nil {
		for _, child := range childrens {
			res = append(res, child.(Node))
		}
	}
	return res
}

// Returns direct dependencies of a node
func (m *objectMap) GetDependencies(nodeID string) NodeList {
	res := NodeList{}
	if parents, err := m.dag.GetParents(nodeID); err == nil {
		for _, parent := range parents {
			res = append(res, parent.(Node))
		}
	}
	return res
}

// Order all objects following their dependencies
func (m *objectMap) OrderedDependencies() []Node {
	visitor := &NodeVisitor{}
	m.dag.OrderedWalk(visitor)
	return visitor.nodes
}

type NodeVisitor struct {
	nodes []Node
}

// For any edge a -> b (eg. b depends on a), node a will be visited before node b.
func (nv *NodeVisitor) Visit(v dag.Vertexer) {
	_, value := v.Vertex()
	nv.nodes = append(nv.nodes, value.(Node))
}

// Process all objects to check if they are Ready as well as all their dependencies
// If node name is provided, process only the dependents of this node
func (m *objectMap) ProcessReconciledStatus(name string) {
	var dependents ObjectMap
	if name != "" {
		dag, _, err := m.dag.GetDescendantsGraph(name)
		if err == nil {
			dependents = MapFrom(dag)
		} else {
			dependents = m
		}
	} else {
		dependents = m
	}
	for _, obj := range dependents.OrderedDependencies() {
		if !obj.Ready() {
			obj.setReconciled(false)
			continue
		}
		allDependenciesReconciled := true
		for _, dep := range m.GetDependencies(obj.QualifiedName()) {
			if !dep.Reconciled() {
				allDependenciesReconciled = false
				break
			}
		}
		obj.setReconciled(allDependenciesReconciled)
	}
}

// Sort the list of objects by their order of dependency in a deterministic way
func (m *objectMap) SortDependencies(source NodeList) (result NodeList) {
	sort.Sort(source)
	added := map[string]bool{}
	for {
	Outer:
		for _, node := range source {
			nodeName := node.QualifiedName()
			if added[nodeName] {
				continue // object is already in result
			}
			nodeDeps, _ := m.dag.GetParents(nodeName)
			for dep := range nodeDeps {
				if !added[dep] {
					continue Outer // wait for all dependencies to be stored in result
				}
			}
			result = append(result, node)
			added[nodeName] = true
		}
		if len(result) == len(source) {
			break
		}
	}
	for _, node := range result {
		node.setWatched(true)
	}
	return result
}

// Mark all the dependencies of watched Nodes as watched, and returns the node list
// if rootNode is empty, return all node of the Map
// if rootNode is not found in the original Map, returns an empty list
func (m *objectMap) FilterDependencies(rootNode string) (deps NodeList) {
	for _, node := range m.dag.GetVertices() {
		node.(Node).setWatched(rootNode == "")
	}
	if rootNode == "" {
		return m.Items()
	}
	root, found := m.Get(rootNode)
	if !found {
		return deps
	}
	// We use GetAncestors instead of AncestorsWalker as this function
	// is maintaining an internal cache that could improve subsequent calls
	watchedDeps, err := m.dag.GetAncestors(rootNode)
	watchedDeps[rootNode] = root
	if err != nil {
		return deps
	}
	for _, dep := range watchedDeps {
		for _, hrName := range GetChildHelmReleases(dep.(Node)) {
			if _, found := watchedDeps[hrName]; !found {
				if hr, found := m.Get(hrName); found {
					watchedDeps[hrName] = hr
					// add all dependencies of the HR
					hrDeps, err := m.dag.GetAncestors(hrName)
					if err != nil {
						continue // unlikely as we already got hrName from dag
					}
					for _, hrDep := range hrDeps {
						watchedDeps[hrDep.(Node).QualifiedName()] = hrDep
					}
				}
			}
		}
	}
	for _, watched := range watchedDeps {
		watched.(Node).setWatched(true)
		deps = append(deps, watched.(Node))
	}
	return deps
}

// Determine if some object depends on another (eventually indirectly)
func (m *objectMap) DependsOn(dependent string, dependency string) bool {
	// We use GetAncestors instead of AncestorsWalker as this function is expected
	// to be essentially used to determine if watched resource depends on a given resource.
	// Consequently we expect to leverage GetAncestors cache on subsequent calls
	allDeps, err := m.dag.GetAncestors(dependent)
	if err != nil {
		return false
	}
	_, found := allDeps[dependency]
	return found
}

// Reprocess dependencies of all nodes that have missingDependencies
func (m *objectMap) UpdateAllDependencies() error {
	var err error
	for _, obj := range m.dag.GetVertices() {
		if obj.(Node).HasMissingDependencies() {
			err = errors.Join(err, m.UpdateDependencies(obj.(Node)))
		}
	}
	return err
}

// Update edges from a given node following FluxObject's dependencies
func (m *objectMap) UpdateDependencies(node Node) error {
	missingDependencies := []string{}
	newDependencies := node.ListDependencies()
	// First remove any deleted dependency
	for _, existingDep := range m.GetDependencies(node.QualifiedName()) {
		found := false
		for _, newDep := range newDependencies {
			if newDep == existingDep.QualifiedName() {
				found = true
				break
			}
		}
		if !found {
			if err := m.dag.DeleteEdge(existingDep.QualifiedName(), node.QualifiedName()); err != nil {
				return err
			}
		}
	}
	// Then reprocess all dependencies
	for _, dep := range newDependencies {
		err := m.dag.AddEdge(dep, node.QualifiedName())
		if err == nil {
			continue
		}
		switch err.(type) {
		case dag.EdgeDuplicateError:
			continue
		case dag.IDUnknownError:
			missingDependencies = append(missingDependencies, dep)
		case dag.SrcDstEqualError:
			return fmt.Errorf("object %s can't depend on itself", node.QualifiedName())
		case dag.EdgeLoopError:
			path, _ := m.getDependencyPath(dep, node.QualifiedName(), []string{node.QualifiedName()})
			log.Printf("object %s can't depend on %s as it would create a cycle: %s", node.QualifiedName(), dep, strings.Join(path, " -> "))
			// Mark this dependency as missing to ensure that it will be reprocessed on future topology changes
			missingDependencies = append(missingDependencies, dep)
		default:
			return fmt.Errorf("encountered unexpected error while processing dependency %s of node %s: %s", dep, node.QualifiedName(), err)
		}
	}
	node.SetMissingDependencies(missingDependencies)
	return nil
}

func (m *objectMap) getDependencyPath(dependent string, dependency string, path []string) ([]string, bool) {
	path = append(path, dependent)
	if dependencies, err := m.dag.GetParents(dependent); err == nil {
		for dep := range dependencies {
			if dep == dependency {
				path = append(path, dependency)
				return path, true
			} else {
				if path, found := m.getDependencyPath(dep, dependency, path); found {
					return path, true
				}
			}
		}
	}
	return path, false
}

// Node
func NewNode(object FluxObject) Node {
	node := node{
		FluxObject: object,
	}
	return &node
}

func (n *node) GetObject() FluxObject {
	n.RLock()
	defer n.RUnlock()
	return n.FluxObject
}

func (n *node) setObject(obj FluxObject) {
	n.Lock()
	defer n.Unlock()
	n.FluxObject = obj
}

// A FluxObject is considered as Reconciled if it is Ready
// as well as all its direct and indirect dependencies
func (n *node) Reconciled() bool {
	n.RLock()
	defer n.RUnlock()
	return n.reconciled
}

func (n *node) setReconciled(reconciled bool) {
	n.Lock()
	defer n.Unlock()
	n.reconciled = reconciled
}

func (n *node) Watched() bool {
	n.RLock()
	defer n.RUnlock()
	return n.watched
}

func (n *node) setWatched(watched bool) {
	n.Lock()
	defer n.Unlock()
	n.watched = watched
}

// Sort interface for NodeList
func (m NodeList) Len() int {
	return len(m)
}

func (m NodeList) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}

func (m NodeList) Less(i, j int) bool {
	return m[i].QualifiedName() < m[j].QualifiedName()
}
