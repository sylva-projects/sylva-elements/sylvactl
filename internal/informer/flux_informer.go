/*
Copyright 2023 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package informer

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"
	"time"

	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"

	corev1 "k8s.io/api/core/v1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/tools/cache"
	"sigs.k8s.io/cli-utils/pkg/object"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"github.com/fluxcd/flux2/v2/pkg/manifestgen"
	helmv2 "github.com/fluxcd/helm-controller/api/v2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"
	"github.com/fluxcd/pkg/apis/meta"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	sourcev1beta2 "github.com/fluxcd/source-controller/api/v1beta2"

	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/recorder"
	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/utils"
)

const (
	DateFormat                   = "2006-01-02T15:04:05"
	maxAnnotations               = 10
	reconcileAnnotationPrefix    = "sylvactl/"
	ReconcileStartedAnnotation   = reconcileAnnotationPrefix + "reconcileStartedAt"
	ReconcileCompletedAnnotation = reconcileAnnotationPrefix + "reconcileCompletedAt"
)

type TreeInformer struct {
	context.Context
	context.CancelFunc
	sync.WaitGroup
	client.Client
	Initialized         bool
	ReconcileDependents bool
	ResumeSuspended     bool
	lock                sync.Mutex
	ObjectMap
	ObjEvents        chan ObjEvent
	eventQueue       chan informerEvent
	recorder         recorder.Recorder
	defaultNamespace string
	runIdentifier    string
}

type ObjEvent struct {
	FluxObject
	Delete bool
}

type informerEvent struct {
	object interface{}
	delete bool
}

var wathedResources = []schema.GroupVersionResource{
	kustomizev1.GroupVersion.WithResource("kustomizations"),
	helmv2.GroupVersion.WithResource("helmreleases"),
	sourcev1.GroupVersion.WithResource("gitrepositories"),
	sourcev1.GroupVersion.WithResource("helmrepositories"),
	sourcev1.GroupVersion.WithResource("helmcharts"),
	sourcev1beta2.GroupVersion.WithResource("ocirepositories"),
}

func NewScheme() *runtime.Scheme {
	scheme := runtime.NewScheme()
	_ = corev1.AddToScheme(scheme)
	_ = apiextensionsv1.AddToScheme(scheme)
	_ = kustomizev1.AddToScheme(scheme)
	_ = helmv2.AddToScheme(scheme)
	_ = sourcev1.AddToScheme(scheme)
	_ = sourcev1.AddToScheme(scheme)
	return scheme
}

func CheckFluxCRDs(ctx context.Context, kubeClient client.Client, retryTimeout time.Duration) error {
	crdList := &apiextensionsv1.CustomResourceDefinitionList{}
	selector := client.MatchingLabels{manifestgen.PartOfLabelKey: manifestgen.PartOfLabelValue}
	// Retry if there's a transient API error
	backoff := retry.DefaultBackoff
	backoff.Duration = retryTimeout
	err := retry.OnError(
		backoff,
		func(err error) bool {
			return err != nil
		},
		func() error {
			return kubeClient.List(ctx, crdList, selector)
		})
	if err != nil {
		return err
	}
outer:
	for _, gvr := range wathedResources {
		grName := gvr.GroupResource().String()
		for _, crd := range crdList.Items {
			if crd.Name == grName {
				continue outer
			}
		}
		return fmt.Errorf("flux CRD %s not declared in API", grName)
	}
	return nil
}

func NewInformer(ctx context.Context, config *rest.Config, objEvents chan ObjEvent, watchedResource string,
	reconcileDependents bool, resumeSuspended bool, retryTimeout time.Duration, namespace string, recordFile string) (*TreeInformer, error) {

	// build a standard kubeClient for most of operations
	cl, err := dynamic.NewForConfig(config)
	if err != nil {
		return nil, fmt.Errorf("failed to create client: %s", err)
	}
	kubeClient, err := client.New(config, client.Options{Scheme: NewScheme()})
	if err != nil {
		return nil, err
	}

	// Fail promptly if there is an API connectivity issue
	{
		// build a kubeClient with a short timeout for the initial CheckFluxCRDs call
		shortTimeoutConfig := rest.CopyConfig(config)
		shortTimeoutConfig.Timeout = 2 * time.Second
		shortTimeoutKubeClient, err := client.New(shortTimeoutConfig, client.Options{Scheme: NewScheme()})
		if err != nil {
			return nil, err
		}

		if err := CheckFluxCRDs(ctx, shortTimeoutKubeClient, retryTimeout); err != nil {
			return nil, err
		}
	}

	ctx, cancel := context.WithCancel(ctx)
	tree := TreeInformer{
		Context:             ctx,
		CancelFunc:          cancel,
		Client:              kubeClient,
		Initialized:         false,
		ReconcileDependents: reconcileDependents,
		ResumeSuspended:     resumeSuspended,
		ObjectMap:           NewMap(),
		ObjEvents:           objEvents,
		eventQueue:          make(chan informerEvent, 10_000),
		defaultNamespace:    namespace,
		runIdentifier:       utils.RandomString(6),
	}

	if tree.recorder, err = recorder.NewRecorder(recordFile); err != nil {
		return nil, err
	}

	factory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(cl, time.Minute, namespace, nil)
	handlers := cache.ResourceEventHandlerFuncs{
		AddFunc:    tree.resourceAdded,
		UpdateFunc: tree.resourceUpdated,
		DeleteFunc: tree.resourceDeleted,
	}
	go tree.eventHandler() // Used to buffer events from handlers

	for _, gvr := range wathedResources {
		informer := factory.ForResource(gvr)
		if _, err := informer.Informer().AddEventHandler(handlers); err != nil {
			return nil, err
		}
	}
	factory.Start(ctx.Done())
	// Wait for all the internal machinery to warm up and for cache so sync
	for gvr, ok := range factory.WaitForCacheSync(ctx.Done()) {
		if !ok {
			return nil, fmt.Errorf("failed to sync cache for resource %v", gvr)
		}
	}
	// Wait for all initial events to be handled
	for len(tree.eventQueue) > 0 {
		time.Sleep(10 * time.Millisecond)
	}
	if err = tree.recorder.RecordInformerSync(); err != nil {
		return nil, err
	}
	// As UpdateDependencies processing is disabled during cache sync, do it once we have a complete view of the system.
	if err := tree.ObjectMap.UpdateAllDependencies(); err != nil {
		log.Printf("Some errors were encountered during the initial processing of nodes dependencies: %s\n", err)
	}
	// Mark active resources
	tree.ObjectMap.FilterDependencies(watchedResource)
	// Same for ProcessReconciledStatus
	tree.ObjectMap.ProcessReconciledStatus("")
	// Then tree can be considered as initialized
	tree.lock.Lock()
	tree.Initialized = true
	tree.lock.Unlock()
	// Request reconciliation of objects that are reconcilable
	for _, obj := range tree.ObjectMap.Items() {
		if tree.Reconcilable(obj) && !obj.Ready() && obj.Watched() {
			tree.setStartReconcile(obj)
		}
	}
	return &tree, nil
}

func convertObject(u *unstructured.Unstructured, obj FluxAdapter) FluxAdapter {
	if err := runtime.DefaultUnstructuredConverter.FromUnstructured(u.UnstructuredContent(), obj.ClientObject()); err != nil {
		log.Printf("Failed to convert unstructured %s to %s\n",
			object.UnstructuredToObjMetadata(u).String(), obj.GetObjectKind().GroupVersionKind().Kind)
	}
	return obj
}

func castObject(object *unstructured.Unstructured) FluxAdapter {
	switch object.GetKind() {
	case kustomizev1.KustomizationKind:
		return convertObject(object, kustomizationAdapter{&kustomizev1.Kustomization{}})
	case helmv2.HelmReleaseKind:
		return convertObject(object, helmReleaseAdapter{&helmv2.HelmRelease{}})
	case sourcev1.HelmChartKind:
		return convertObject(object, helmChartAdapter{&sourcev1.HelmChart{}})
	case sourcev1.HelmRepositoryKind:
		return convertObject(object, helmRepositoryAdapter{&sourcev1.HelmRepository{}})
	case sourcev1.GitRepositoryKind:
		return convertObject(object, gitRepositoryAdapter{&sourcev1.GitRepository{}})
	case sourcev1beta2.OCIRepositoryKind:
		return convertObject(object, ociRepositoryAdapter{&sourcev1beta2.OCIRepository{}})
	default:
		return nil // Informer is not supposed to notify about other objects
	}
}

func (t *TreeInformer) resourceAdded(obj interface{}) {
	select {
	case t.eventQueue <- informerEvent{object: obj, delete: false}:
	default:
		log.Println("Failed to handle event, queue is full")
	}
}

func (t *TreeInformer) resourceUpdated(old, new interface{}) {
	select {
	case t.eventQueue <- informerEvent{object: new, delete: false}:
	default:
		log.Println("Failed to handle event, queue is full")
	}
}

func (t *TreeInformer) resourceDeleted(obj interface{}) {
	select {
	case t.eventQueue <- informerEvent{object: obj, delete: true}:
	default:
		log.Println("Failed to handle event, queue is full")
	}
}

func (t *TreeInformer) eventHandler() {
	t.WaitGroup.Add(1)
	defer func() {
		// Stop recorder in defer instead of in <-t.Context.Done() case
		// to ensure that it will be called even if goroutine panics
		t.recorder.Stop()
		t.WaitGroup.Done()
	}()
	for {
		select {
		case <-t.Context.Done():
			return
		case treeEvent := <-t.eventQueue:
			if u, ok := treeEvent.object.(*unstructured.Unstructured); ok {
				if err := t.recorder.RecordEvent(u, treeEvent.delete); err != nil {
					log.Printf("Failed to record event: %v", err)
				}
				t.handleEvent(NewFluxObject(castObject(u), t.defaultNamespace),
					treeEvent.delete)
			}
		}
	}
}

func (t *TreeInformer) Stop() {
	t.CancelFunc()
	t.WaitGroup.Wait()
}

func (t *TreeInformer) handleEvent(obj FluxObject, delete bool) {
	t.lock.Lock()
	defer t.lock.Unlock()

	if delete {
		if err := t.ObjectMap.Remove(obj); err != nil {
			log.Printf("Encountered errors while removing object %s: %s", obj.QualifiedName(), err)
		}
		t.ObjectMap.ProcessReconciledStatus("")
		t.dispatchEvent(obj, true)
		return
	}

	prev, node, err := t.ObjectMap.Store(obj, t.Initialized)
	if err != nil {
		log.Printf("Failed to store object %s: %s", obj.QualifiedName(), err)
	}
	if t.Initialized {
		if prev == nil || prev.Ready() != obj.Ready() {
			t.ObjectMap.ProcessReconciledStatus(obj.QualifiedName())
		}
		if t.Reconcilable(node) && !obj.Ready() && node.Watched() {
			t.setStartReconcile(obj)
		}
		if obj.Ready() && (prev == nil || !prev.Ready()) {
			t.setReconcileCompletedAnnotations(obj)
			t.NotifyReconcilableDependents(obj)
		}
		// Notify client of object changes
		if prev == nil || obj.Ready() != prev.Ready() || obj.Status() != prev.Status() ||
			obj.GetLastHandledReconcileAt() != prev.GetLastHandledReconcileAt() {
			t.dispatchEvent(obj, false)
		}
	}
}

func (t *TreeInformer) dispatchEvent(obj FluxObject, delete bool) {
	select {
	case t.ObjEvents <- ObjEvent{FluxObject: obj, Delete: delete}:
	default:
		log.Println("Failed to dispatch event, queue is full")
	}
}

func (t *TreeInformer) Reconcilable(node Node) bool {
	if node.Ready() || (node.Suspended() && !t.ResumeSuspended) || node.HasMissingDependencies() {
		return false
	} else {
		for _, dep := range t.GetDependencies(node.QualifiedName()) {
			if !dep.Reconciled() {
				return false
			}
		}
		return true
	}
}

func (t *TreeInformer) NotifyReconcilableDependents(object FluxObject) {
	for _, node := range t.ObjectMap.GetDependents(object.QualifiedName()) {
		if t.Reconcilable(node) {
			t.setStartReconcile(node)
		}
	}
}

// We build a reconcileID using the identifier of current execution and the object generation
// It ensures that a given generation won't be reconciled more than one time per execution of sylvactl,
// but still enables a given generation to be reconciled in another execution, which can be desirable
// if some secrets or configMaps changed but not the HelmReleases of Kustomizations using them.
func (t *TreeInformer) getReconcileID(object client.Object) string {
	return fmt.Sprintf(".%d.%s", object.GetGeneration(), t.runIdentifier)
}

// Returns the duration of current reconcile request, based on reconcile annotations
// It computes the time elapsed since ReconcileStartedAnnotation, if it was set with current reconcileID, otherwise it returns 0
func (t *TreeInformer) CurrentReconcileDuration(obj FluxObject) time.Duration {
	reconcileStartedAnnotation := ReconcileStartedAnnotation + t.getReconcileID(obj)
	reconcileStarted := obj.GetAnnotationValue(reconcileStartedAnnotation)
	if reconcileStarted == "" {
		return 0
	}
	reconcileStartedDate, err := time.Parse(DateFormat, reconcileStarted)
	if err != nil {
		return 0
	}
	return time.Since(reconcileStartedDate)
}

// This method, if reconcile start annotation does not yet exist for current identifier:
// * sets this annotation
// * if --reconcile option is set, requests flux reconciliation
// * if --resume-suspended options is set, resumes the resource if it is suspended
func (t *TreeInformer) setStartReconcile(object FluxObject) {
	reconcileDate := time.Now().UTC().Format(DateFormat)
	reconcileStartedAnnotation := ReconcileStartedAnnotation + t.getReconcileID(object)
	if object.GetAnnotationValue(reconcileStartedAnnotation) == "" {
		annotations := map[string]string{
			reconcileStartedAnnotation: reconcileDate,
		}
		if t.ReconcileDependents {
			annotations[meta.ReconcileRequestAnnotation] = reconcileDate
			// Force-reconcile helmRelease if it failed to install or upgrade
			if object.GetObjectKind().GroupVersionKind().Kind == helmv2.HelmReleaseKind {
				if readyCond := apimeta.FindStatusCondition(object.GetConditions(), meta.ReadyCondition); readyCond != nil &&
					(readyCond.Reason == helmv2.InstallFailedReason || readyCond.Reason == helmv2.UpgradeFailedReason) {
					annotations[helmv2.ForceRequestAnnotation] = reconcileDate
				}
			}
		}
		t.WaitGroup.Add(1)
		go t.UpdateObject(
			object,
			annotations,
			(t.ResumeSuspended && object.Suspended()),
		)
	}
}

func (t *TreeInformer) setReconcileCompletedAnnotations(object FluxObject) {
	reconcileID := t.getReconcileID(object)
	if object.GetAnnotationValue(ReconcileStartedAnnotation+reconcileID) != "" &&
		object.GetAnnotationValue(ReconcileCompletedAnnotation+reconcileID) == "" {
		t.WaitGroup.Add(1)
		go t.UpdateObject(object, map[string]string{
			ReconcileCompletedAnnotation + reconcileID: time.Now().UTC().Format(DateFormat),
		}, false)
	}
}

func (t *TreeInformer) UpdateObject(fluxObject FluxObject, annotations map[string]string, resume bool) {
	defer t.WaitGroup.Done()
	objName := fluxObject.QualifiedName()
	err := retry.RetryOnConflict(retry.DefaultBackoff, func() (err error) {
		newFluxObj := fluxObject.DeepCopy()
		object := newFluxObj.ClientObject()
		patch := client.MergeFrom(object.DeepCopyObject().(client.Object))
		if ann := object.GetAnnotations(); ann == nil {
			object.SetAnnotations(annotations)
		} else {
			for annotationName, annotationValue := range annotations {
				ann[annotationName] = annotationValue
			}
			cleanOldReconcileAnnotations(ann, t.getReconcileID(object))
			object.SetAnnotations(ann)
		}
		if resume {
			log.Printf("Resuming %s", objName)
			newFluxObj.setSuspend(false)
		}
		return t.Client.Patch(t.Context, object, patch)
	})
	if err != nil {
		log.Printf("Failed to update object %s: %s", objName, err)
	}
}

// Remove older reconcile annotations to avoid accumulating them forever
func cleanOldReconcileAnnotations(annotations map[string]string, reconcileID string) {
	sylvactlAnnotations := sortableAnnotations{}
	for name, value := range annotations {
		if strings.HasPrefix(name, reconcileAnnotationPrefix) {
			if date, err := time.Parse(DateFormat, value); err == nil {
				sylvactlAnnotations = append(sylvactlAnnotations, sortableAnnotation{
					name: name,
					date: date,
				})
			}
		}
	}
	if len(sylvactlAnnotations) > maxAnnotations {
		sort.Sort(sylvactlAnnotations)
		for i := 0; i < len(sylvactlAnnotations)-maxAnnotations; i++ {
			delete(annotations, sylvactlAnnotations[i].name)
		}
	}
}

type sortableAnnotation struct {
	name string
	date time.Time
}
type sortableAnnotations []sortableAnnotation

func (s sortableAnnotations) Len() int {
	return len(s)
}

func (s sortableAnnotations) Less(i, j int) bool {
	return s[i].date.Before(s[j].date)
}

func (s sortableAnnotations) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
