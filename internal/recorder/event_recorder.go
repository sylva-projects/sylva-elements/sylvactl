/*
Copyright 2023 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package recorder

import (
	"fmt"
	"os"
	"time"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sigs.k8s.io/yaml"
)

const (
	RecordDateFormat   = "2006-01-02T15:04:05.000"
	YamlDocSeparator   = "---"
	InformerSyncMarker = "# informer synced"
)

type Recorder interface {
	RecordInformerSync() error
	RecordEvent(*unstructured.Unstructured, bool) error
	Stop()
}

type recorder struct {
	*os.File
}

type EventRecord struct {
	Date   string                 `json:"date,omitempty"`
	Delete bool                   `json:"delete,omitempty"`
	Object map[string]interface{} `json:"object,omitempty"`
}

func NewRecorder(fileName string) (Recorder, error) {
	if fileName == "" {
		return &recorder{nil}, nil
	}
	recordFile, err := os.Create(fileName)
	if err != nil {
		return nil, fmt.Errorf("failed to create event recording file %s:  %v", fileName, err)
	}
	return &recorder{recordFile}, nil
}

func (recorder *recorder) RecordInformerSync() error {
	if recorder.File != nil {
		if _, err := recorder.WriteString(fmt.Sprintf("%s\n%s\n", InformerSyncMarker, YamlDocSeparator)); err != nil {
			return err
		}
	}
	return nil
}

func (recorder *recorder) RecordEvent(u *unstructured.Unstructured, delete bool) error {

	if recorder.File != nil {
		record := EventRecord{
			Date:   time.Now().Format(RecordDateFormat),
			Delete: delete,
			Object: u.Object,
		}
		// Drop managedFields to save space
		unstructured.RemoveNestedField(record.Object, "metadata", "managedFields")

		data, err := yaml.Marshal(record)
		if err != nil {
			return err
		}
		if _, err = recorder.Write(data); err != nil {
			return err
		}
		if _, err = recorder.WriteString(YamlDocSeparator + "\n"); err != nil {
			return err
		}
	}
	return nil
}

func (recorder *recorder) Stop() {
	if recorder.File != nil {
		recorder.Close()
	}
}
