/*
Copyright 2025 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package testenv

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/util/retry"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"

	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/recorder"
)

type Player struct {
	ctx           context.Context
	client        client.Client
	file          *os.File
	err           chan error
	synced        chan bool
	nsMap         map[string]bool
	prevEventDate time.Time
}

func NewRecordPlayer(ctx context.Context, client client.Client, filePath string) (*Player, error) {

	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	player := &Player{
		ctx:    ctx,
		client: client,
		file:   file,
		err:    make(chan error),
		synced: make(chan bool),
		nsMap:  map[string]bool{},
	}

	go player.run()
	return player, nil
}

// Fill the API with the initial list of objects
func (p *Player) WaitForCacheSync() error {
	select {
	case <-p.synced:
		return nil
	case err := <-p.err:
		return err
	}
}

// Resume the playback of the sequence of events observed after initial sync
func (p *Player) Resume() {
	p.synced <- true
}

func (p *Player) Error() <-chan error {
	return p.err
}

func (p *Player) run() {
	scanner := bufio.NewScanner(p.file)
	var currentBlock strings.Builder

	for {
		select {
		case <-p.ctx.Done():
			if err := p.file.Close(); err != nil {
				p.err <- err
			}
			return
		default:
			if !scanner.Scan() {
				if err := p.processEventBlock(&currentBlock); err != nil {
					p.err <- err
				}
				// Playback is complete, schedule an error to fail the test early
				// if event processing has not matched that envent
				time.Sleep(2 * time.Second)
				p.err <- fmt.Errorf("record playback completed since more than 2 seconds, event processing should be finished")
				return
			}
			line := scanner.Text()
			switch line {
			case recorder.InformerSyncMarker:
				// Notify and wait for synchronization
				p.synced <- true
				// Wait for informer to sync
				<-p.synced
			case recorder.YamlDocSeparator:
				if err := p.processEventBlock(&currentBlock); err != nil {
					p.err <- err
				}
			default:
				currentBlock.WriteString(line + "\n")
			}
		}
	}
}

func (p *Player) processEventBlock(currentBlock *strings.Builder) error {
	if currentBlock.Len() == 0 {
		return nil
	}
	defer currentBlock.Reset()

	var record recorder.EventRecord
	err := yaml.Unmarshal([]byte(currentBlock.String()), &record)
	if err != nil {
		return fmt.Errorf("failed to unmarshal event record: %w", err)
	}

	eventDate, err := time.Parse(recorder.RecordDateFormat, record.Date)
	if err != nil {
		return err
	}
	if !p.prevEventDate.IsZero() {
		// enforce delay between events, but limit delay to 10ms to speed-up replay
		time.Sleep(min(10*time.Millisecond, eventDate.Sub(p.prevEventDate)))
	}
	p.prevEventDate = eventDate

	obj := unstructured.Unstructured{Object: record.Object}
	unstructured.RemoveNestedField(obj.Object, "metadata", "uid")

	if obj.GetNamespace() != "" {
		if err = p.ensureNamespace(obj.GetNamespace()); err != nil {
			return err
		}
	}

	err = p.updateObject(obj.DeepCopy(), false)
	if errors.IsNotFound(err) {
		unstructured.RemoveNestedField(obj.Object, "metadata", "resourceVersion")
		err = p.client.Create(p.ctx, obj.DeepCopy())
	}
	if err != nil {
		return err
	}
	// update status
	return p.updateObject(&obj, true)
}

func (p *Player) updateObject(obj *unstructured.Unstructured, updateStatus bool) error {

	return retry.RetryOnConflict(retry.DefaultBackoff, func() (err error) {
		existing := obj.DeepCopy()
		if err = p.client.Get(p.ctx, client.ObjectKeyFromObject(existing), existing); err != nil {
			return err
		}
		if err = unstructured.SetNestedField(obj.Object, existing.GetResourceVersion(), "metadata", "resourceVersion"); err != nil {
			return err
		}
		if updateStatus {
			return p.client.Status().Update(p.ctx, obj)
		} else {
			return p.client.Update(p.ctx, obj)
		}
	})
}

func (p *Player) ensureNamespace(name string) error {

	if _, found := p.nsMap[name]; !found {
		err := p.client.Create(p.ctx, &corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
		})
		if err != nil {
			return err
		}
		p.nsMap[name] = true
	}
	return nil
}
