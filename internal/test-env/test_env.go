/*
Copyright 2025 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package testenv

import (
	"fmt"
	"path/filepath"
	"runtime"

	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"

	helmv2 "github.com/fluxcd/helm-controller/api/v2beta2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
)

type TestEnv struct {
	env    *envtest.Environment
	config *rest.Config
	client client.Client
}

func NewEnv() (*TestEnv, error) {

	testEnv := &envtest.Environment{
		CRDDirectoryPaths:     []string{FilePath("flux-crds")},
		ErrorIfCRDPathMissing: true,

		// The BinaryAssetsDirectory is only required if you want to run the tests directly
		// without call the makefile target test. If not informed it will look for the
		// default path defined in controller-runtime which is /usr/local/kubebuilder/.
		// Note that you must have the required binaries setup under the bin directory to perform
		// the tests directly. When we run make test it will be setup and used automatically.
		// renovate: datasource=github-tags depName=kubernetes-sigs/controller-tools VersionTemplate=envtest-v
		BinaryAssetsDirectory: FilePath("..", "..", "bin", "k8s",
			fmt.Sprintf("1.30.0-%s-%s", runtime.GOOS, runtime.GOARCH)),
	}

	cfg, err := testEnv.Start()
	if err != nil {
		return nil, err
	}

	if err = sourcev1.AddToScheme(scheme.Scheme); err != nil {
		return nil, err
	}
	if err = kustomizev1.AddToScheme(scheme.Scheme); err != nil {
		return nil, err
	}
	if err = helmv2.AddToScheme(scheme.Scheme); err != nil {
		return nil, err
	}
	if err = apiextensionsv1.AddToScheme(scheme.Scheme); err != nil {
		return nil, err
	}
	client, err := client.New(cfg, client.Options{Scheme: scheme.Scheme})
	if err != nil {
		return nil, err
	}
	return &TestEnv{
		env:    testEnv,
		config: cfg,
		client: client,
	}, nil
}

func (t *TestEnv) Client() client.Client {
	return t.client
}

func (t *TestEnv) ClientConfig() *rest.Config {
	return t.config
}

func (t *TestEnv) Stop() error {
	return t.env.Stop()
}

func FilePath(elements ...string) string {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return "unable to get the current filename"
	}
	return filepath.Join(filepath.Dir(filename), filepath.Join(elements...))
}
