#!/bin/sh
set -eo pipefail

# Function to display help message
Help() {
   echo "Project packages cleanup"
   echo
   echo "Syntax: packages_cleanup [-h] -c DAYS_SINCE_CREATION -d DAYS_SINCE_LAST_DOWNLOAD -r PACKAGE_RELEASE_TO_REMOVE"
   echo "options:"
   echo "h     Print this Help."
   echo "c     Days since creation"
   echo "d     Days since last download"
   echo "r     Release to remove"
   echo
}

# Parse command line options
while getopts ":h:c:d:r:" option; do
   case $option in
      h) Help; exit ;;
      c) DAYS_SINCE_CREATION=$OPTARG ;;
      d) DAYS_SINCE_LAST_DOWNLOAD=$OPTARG ;;
      r) PACKAGE_RELEASE_TO_REMOVE=$OPTARG ;;
      \?) echo "Error: Invalid option"; Help; exit 1 ;;
   esac
done

# Check if all required options are provided
if [ -z "$DAYS_SINCE_CREATION" ] || [ -z "$DAYS_SINCE_LAST_DOWNLOAD" ] || [ -z "$PACKAGE_RELEASE_TO_REMOVE" ]; then
    Help
    exit 1
fi

# Variables replaced by command args. Local defined for troubleshooting.
# DAYS_SINCE_CREATION=50
# DAYS_SINCE_LAST_DOWNLOAD=30
# PACKAGE_RELEASE_TO_REMOVE="0.0.0-git"

PACKAGES_OLDER_THAN=$(TZ=GMT+$((24*DAYS_SINCE_CREATION)) date +"%Y-%m-%dT%H:%M:%S.%3NZ")
PACKAGES_LAST_DOWNLOAD=$(TZ=GMT+$((24*DAYS_SINCE_LAST_DOWNLOAD)) date +"%Y-%m-%dT%H:%M:%S.%3NZ")
BASE_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages"

# Initialize files
rm -f packages_to_remove.txt

# Retrieving the packages intended to purge
echo "Retrieving the packages that will be deleted from $CI_PROJECT_TITLE GitLab project created before $PACKAGES_OLDER_THAN and last downloaded before $PACKAGES_LAST_DOWNLOAD  "
glab api projects/${CI_PROJECT_ID}/packages --paginate | \
    jq -c '.[] | select (
        (.version | startswith("'$PACKAGE_RELEASE_TO_REMOVE'")) and 
        .created_at < "'$PACKAGES_OLDER_THAN'" and 
        (.last_downloaded_at < "'$PACKAGES_LAST_DOWNLOAD'" or 
        .last_downloaded_at == null)
    )' | jq -r .id >> packages_to_remove.txt

# Start purging old packages
echo "Starting purge of old packages"
set +e
if [ -s packages_to_remove.txt ]; then
   NUM_PACKAGES=$(wc -l < packages_to_remove.txt)
   echo "$NUM_PACKAGES packages will be removed."
   while read -r PACKAGE_ID; do
      RESPONSE=$(curl --request DELETE --header "JOB-TOKEN: ${CI_JOB_TOKEN}" "$BASE_URL/$PACKAGE_ID" -s -w "\n%{http_code}")
      DELETION_STATUS=$(echo "$RESPONSE" | tail -n1)
      RESPONSE_BODY=$(echo "$RESPONSE" | sed '$d')
      if [ "$DELETION_STATUS" -eq 204 ]; then
            echo "Package $PACKAGE_ID has been deleted successfully"
      else
            echo "/!\\ Unable to delete package $PACKAGE_ID - HTTP status: $DELETION_STATUS, Response: $RESPONSE_BODY"
            error_code=1
      fi
   done < packages_to_remove.txt
else
   echo "No package to delete"
fi
set -e
exit ${error_code:-0}
