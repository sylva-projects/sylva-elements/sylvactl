// status.go

package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/informer"
	"k8s.io/client-go/rest"
)

// Define the status command
var statusCmd = &cobra.Command{
	Use:     "status",
	Short:   "Show status summary and exit",
	Long:    `The status command provides a summary of the status of Flux resources and exits.`,
	Example: `# Show status summary sylvactl status`,
	RunE:    statusCmdRun,
}

func init() {
	rootCmd.AddCommand(statusCmd)
}

// statusCmdRun handles the logic for the status command.
func statusCmdRun(cmd *cobra.Command, args []string) error {
	var watchedResource string
	if len(args) >= 1 {
		watchedResource = args[0]
	} else {
		watchedResource = ""
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer cancel()

	cfg, err := kubeconfigArgs.ToRESTConfig()
	if err != nil {
		return err
	}
	// Disable api deprecation warnings
	cfg.WarningHandler = rest.NoWarnings{}

	objUpdate := make(chan informer.ObjEvent, 1000)
	fluxInformer, err := informer.NewInformer(ctx, cfg, objUpdate, watchedResource, false, false, 10*time.Millisecond, *kubeconfigArgs.Namespace, "")
	if err != nil {
		return err
	}
	progressUpdate, _ := computeProgressUpdate(fluxInformer.ObjectMap, watchedResource)
	if len(progressUpdate) == 0 {
		fmt.Println("All Done: all resources are ready!")
	} else {
		fmt.Println("Following resources are not ready:")
		timeoutSummary(cfg, progressUpdate, false)
	}
	return nil
}
