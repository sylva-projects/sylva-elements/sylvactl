/*
Copyright 2023 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"context"
	"os"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"github.com/spf13/cobra"

	testenv "gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/test-env"
)

var env *testenv.TestEnv
var ctx context.Context
var cancel context.CancelFunc

func TestInformer(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Sylvactl informer tests")
}

var _ = BeforeEach(func() {
	log.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	ctx, cancel = context.WithCancel(context.TODO())
	var err error
	env, err = testenv.NewEnv()
	Expect(err).NotTo(HaveOccurred())
	Expect(env).NotTo(BeNil())
})

var _ = AfterEach(func() {
	By("tearing down the test environment")
	cancel()
	err := env.Stop()
	Expect(err).NotTo(HaveOccurred())
})

var _ = Describe("sylvactl informer tests", func() {

	Context("When replaying capd bootstrap record", func() {

		It("Handles resources statuses as expected", func() {

			By("Start the playback of test scenario")

			player, err := testenv.NewRecordPlayer(ctx, env.Client(), testenv.FilePath("scenarios", "bootstrap-mgt-capd.yaml"))
			Expect(err).NotTo(HaveOccurred())
			Expect(player).NotTo(BeNil())

			err = player.WaitForCacheSync()
			Expect(err).NotTo(HaveOccurred())

			kubeconfigArgs.WrapConfigFn = func(conf *rest.Config) *rest.Config {
				return env.ClientConfig()
			}
			var watchCmd = &cobra.Command{}

			// Configure a record file to test the event recording
			statusCmdArgs.record = testenv.FilePath("scenarios", "test-record-file.yaml")

			// We have to schedule the resume of playback prior to launch command, otherwise it'll remain stuck...
			go func() {
				time.Sleep(2 * time.Second)
				player.Resume()
			}()

			By("Ensure that watch command exits successfully")
			err = watchCmdRun(watchCmd, []string{"Kustomization/sylva-system/management-sylva-units"})
			Expect(err).NotTo(HaveOccurred())

			err = os.Remove(statusCmdArgs.record)
			Expect(err).NotTo(HaveOccurred())
		})

		It("Raises an error if unit is not found", func() {

			By("Start the playback of test scenario")

			player, err := testenv.NewRecordPlayer(ctx, env.Client(), testenv.FilePath("scenarios", "bootstrap-mgt-capd.yaml"))
			Expect(err).NotTo(HaveOccurred())
			Expect(player).NotTo(BeNil())

			err = player.WaitForCacheSync()
			Expect(err).NotTo(HaveOccurred())

			kubeconfigArgs.WrapConfigFn = func(conf *rest.Config) *rest.Config {
				return env.ClientConfig()
			}
			var watchCmd = &cobra.Command{}

			// We have to schedule the resume of playback prior to launch command, otherwise it'll remain stuck...
			go func() {
				time.Sleep(2 * time.Second)
				player.Resume()
			}()

			By("Ensure that watch command raises an error")
			err = watchCmdRun(watchCmd, []string{"Kustomization/sylva-system/fake-unit"})
			Expect(err).To(HaveOccurred())
		})

		It("Raises an error if it fails to connect to the API", func() {

			kubeconfigArgs.WrapConfigFn = func(conf *rest.Config) *rest.Config {
				config := env.ClientConfig()
				config.Host = "127.0.0.1:999"
				return config
			}
			var watchCmd = &cobra.Command{}

			By("Ensure that watch command raises an error")
			err := watchCmdRun(watchCmd, []string{"Kustomization/sylva-system/fake-unit"})
			Expect(err).To(HaveOccurred())
		})

	})
})
