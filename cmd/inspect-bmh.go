package main

import (
	"fmt"
	"net/url"
	"os"
	"reflect"

	"sigs.k8s.io/yaml"

	"github.com/spf13/cobra"
	"github.com/stmcginnis/gofish"
)

var inspectCmd = &cobra.Command{
	Use:   "inspect-bmh",
	Short: "Inspect BaremetalHosts via redfish",
	Long:  `Inspect BaremetalHosts via redfish using server credentials provided or stored in values to retrieve server interfaces and disk details`,
	Example: `# Inspect all BaremetalHosts declared in your environment values
sylvactl inspect-bmh environment-values/my-deployment/values.yaml environment-values/my-deployment/secret.yaml

# Use a wildcard to pass all yamls to the command
sylvactl inspect-bmh environment-values/my-deployment/*

# Inspect a single server without using values files
sylvactl inspect-bmh --host 1.2.3.4 -u root -p changeme -i
`,
	RunE: inspectCmdRun,
}

var cmdHost = bmhDef{}

func init() {
	inspectCmd.Flags().StringVar(&cmdHost.BMHSpec.BMC.Address, "host", "", "Address or hostname of the server, optionally followed by [:port] if different from 443")
	inspectCmd.Flags().StringVarP(&cmdHost.Credentials.Username, "username", "u", "", "redfish/BMC username")
	inspectCmd.Flags().StringVarP(&cmdHost.Credentials.Password, "password", "p", "", "redfish/BMC password")
	inspectCmd.Flags().BoolVarP(&cmdHost.BMHSpec.BMC.DisableCertificateVerification, "insecure", "i", false, "Bypass TLS certificate validation")

	rootCmd.AddCommand(inspectCmd)
}

type confDef struct {
	Cluster clusterDef `json:"cluster"`
}

type clusterDef struct {
	BareMetalHostDefault bmhDef            `json:"baremetal_host_default"`
	BareMetalHosts       map[string]bmhDef `json:"baremetal_hosts"`
}

type bmhDef struct {
	BMHSpec     bmhSpec     `json:"bmh_spec"`
	Credentials credentials `json:"credentials"`
}

type bmhSpec struct {
	BMC bmc `json:"bmc"`
}

type bmc struct {
	Address                        string `json:"address"`
	DisableCertificateVerification bool   `json:"disableCertificateVerification"`
}

type credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func inspectCmdRun(cmd *cobra.Command, args []string) error {

	conf, err := parseConfig(args)
	if err != nil {
		return err
	}

	if cmdHost.BMHSpec.BMC.Address != "" {

		if cmdHost.Credentials.Username == "" || cmdHost.Credentials.Password == "" {
			return fmt.Errorf("username and password must be provided alongside with host option")
		}
		if conf.Cluster.BareMetalHosts == nil {
			conf.Cluster.BareMetalHosts = map[string]bmhDef{}
		}
		// transform address to a parsable url
		address := cmdHost.BMHSpec.BMC.Address
		cmdHost.BMHSpec.BMC.Address = "host://" + address
		conf.Cluster.BareMetalHosts[address] = cmdHost
	}

	for BMHName, BMHDef := range conf.Cluster.BareMetalHosts {

		bmcAddress, err := url.Parse(getDefault(BMHDef.BMHSpec.BMC.Address, conf.Cluster.BareMetalHostDefault.BMHSpec.BMC.Address))
		if err != nil {
			return err
		}
		if bmcAddress.Host == "" {
			return fmt.Errorf("no valid BMC address found for host %s", BMHName)
		}
		endpoint := fmt.Sprintf("https://%s", bmcAddress.Host)

		conf := gofish.ClientConfig{
			Endpoint:  endpoint,
			Username:  getDefault(BMHDef.Credentials.Username, conf.Cluster.BareMetalHostDefault.Credentials.Username),
			Password:  getDefault(BMHDef.Credentials.Password, conf.Cluster.BareMetalHostDefault.Credentials.Password),
			Insecure:  BMHDef.BMHSpec.BMC.DisableCertificateVerification || conf.Cluster.BareMetalHostDefault.BMHSpec.BMC.DisableCertificateVerification,
			BasicAuth: false,
		}

		if err := inspectHost(BMHName, conf); err != nil {
			return err
		}
	}
	return nil
}

func inspectHost(BMHName string, conf gofish.ClientConfig) error {

	fmt.Println("===============================")

	fmt.Printf("Inspecting %s\n", BMHName)

	conn, err := gofish.Connect(conf)
	if err != nil {
		return fmt.Errorf("failed to connect to %s: %v", BMHName, err)
	}
	defer conn.Logout()

	service := conn.Service
	system, err := service.Systems()
	if err != nil {
		return err
	}

	for _, sys := range system {

		fmt.Println("--- Inspecting ethernet interfaces ---")

		nics, err := sys.EthernetInterfaces()
		if err != nil {
			return err
		}
		for _, nic := range nics {
			fmt.Printf("Interface: %s\n", nic.ID)
			fmt.Printf("\tDescription: %s\n", nic.Description)
			fmt.Printf("\tAddress: %s\n", nic.MACAddress)
			fmt.Printf("\tLinkStatus: %s\n", nic.LinkStatus)
			fmt.Printf("\tSpeedMbps: %d\n", nic.SpeedMbps)
		}

		fmt.Println("--- Inspecting disks ---")

		storage, err := sys.Storage()
		if err != nil {
			continue
		}
		for _, ss := range storage {
			drives, err := ss.Drives()
			if err != nil {
				continue
			}

			for i, drive := range drives {
				fmt.Printf("Drive %d\n", i)
				fmt.Printf("\tManufacturer: %s\n", drive.Manufacturer)
				fmt.Printf("\tModel: %s\n", drive.Model)
				fmt.Printf("\tSize: %d GiB\n", (drive.CapacityBytes / 1024 / 1024 / 1024))
				fmt.Printf("\tSerial number: %s\n", drive.SerialNumber)
				fmt.Printf("\tPart number: %s\n", drive.PartNumber)
				fmt.Printf("\tLocation: %s %d\n", drive.PhysicalLocation.PartLocation.LocationType, drive.PhysicalLocation.PartLocation.LocationOrdinalValue)
			}
		}
	}
	return nil
}

func parseConfig(confFiles []string) (*confDef, error) {

	mergedConfig := make(map[string]interface{})

	for _, file := range confFiles {
		confFile := map[string]interface{}{}
		yamlFile, err := os.ReadFile(file)
		if err != nil {
			fmt.Printf("Failed to read file %s: %v", file, err)
		}
		err = yaml.Unmarshal(yamlFile, &confFile)
		if err != nil {
			fmt.Printf("Failed to parse file %s as json: %v", file, err)
		}
		mergeMaps(mergedConfig, confFile)
	}

	merged, err := yaml.Marshal(mergedConfig)
	if err != nil {
		return nil, err
	}

	var conf confDef
	err = yaml.Unmarshal(merged, &conf)
	if err != nil {
		return nil, err
	}
	return &conf, nil
}

func getDefault(value, defaultValue string) string {
	if value != "" {
		return value
	} else {
		return defaultValue
	}
}

func mergeMaps(dst, src map[string]interface{}) {
	for key, srcVal := range src {
		if dstVal, exists := dst[key]; exists {
			srcValType := reflect.TypeOf(srcVal)
			dstValType := reflect.TypeOf(dstVal)

			if srcValType == dstValType {
				if srcValType.Kind() == reflect.Map {
					dstMap := dstVal.(map[string]interface{})
					srcMap := srcVal.(map[string]interface{})
					mergeMaps(dstMap, srcMap)
				} else {
					dst[key] = srcVal
				}
			} else {
				dst[key] = srcVal
			}
		} else {
			dst[key] = srcVal
		}
	}
}
