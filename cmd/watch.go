/*
Copyright 2023 The Sylva authors


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"strings"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"

	helmv2 "github.com/fluxcd/helm-controller/api/v2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"

	"github.com/spf13/cobra"

	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/informer"
	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/inventory"
	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/report"
	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/spinner"
)

var watchCmd = &cobra.Command{
	Use:   "watch",
	Short: "Watch status of Flux resources",
	Long:  `The watch command shows the progress of flux resources reconciliation`,
	Example: `# Watch until all resources become ready
sylvactl watch

# Watch until a specific resource becomes ready with a timeout of 10 minutes
# (and fasten convergence by triggering reconciliation of dependents when a resource becomes ready)

sylvactl watch --reconcile --timeout 10m Kustomization/default/sylva-units`,
	RunE: watchCmdRun,
}

const (
	ReadyMessageAnnotation = "sylvactl/readyMessage"
	UnitReconcileTimeout   = "sylvactl/unitTimeout"
	UnitTimeoutReference   = "sylvactl/timeoutReference"
)

var (
	statusCmdArgs struct {
		ignoreSuspended   bool
		log               bool
		once              bool
		reconcile         bool
		record            string
		save              string
		unitTimeout       time.Duration
		timeout           time.Duration
		unitTimeoutFactor float64
		exitCondition     []string
		skipInventory     bool
		resumeSuspended   bool
		retryTimeout      time.Duration
	}
	ticker       = time.NewTicker(time.Second)
	timeoutError = errors.New("timeout")
)

type nodeMap map[string]informer.Node

func init() {
	watchCmd.Flags().BoolVar(&statusCmdArgs.log, "log", false, "log resources changes rather than updating output (always true if output is not a terminal)")
	watchCmd.Flags().BoolVar(&statusCmdArgs.once, "once", false, "show current resource status and exit")
	watchCmd.Flags().BoolVar(&statusCmdArgs.reconcile, "reconcile", false, "fasten convergence by triggering reconciliation of dependents as soon as a flux resource becomes ready")
	watchCmd.Flags().BoolVar(&statusCmdArgs.ignoreSuspended, "ignore-suspended", false, "neither show nor wait for suspended resources")
	watchCmd.Flags().BoolVar(&statusCmdArgs.skipInventory, "skip-inventory", false, "Skip the display of inventory of stalled resources while exiting on errors")
	watchCmd.Flags().BoolVar(&statusCmdArgs.resumeSuspended, "resume-suspended", false, "Will resume any suspended resource on which the watched resource depends on, when it becomes reconcilable")
	watchCmd.Flags().DurationVar(&statusCmdArgs.unitTimeout, "unit-timeout", 0, "time to wait for flux unit to become ready (it activates the reconcile option). Non-zero values should contain a corresponding time unit (e.g., 1s, 2m, 3h). A value of zero (the default) means no timeout.")
	watchCmd.Flags().DurationVar(&statusCmdArgs.timeout, "timeout", 0, "time to wait for flux resources to become ready. Non-zero values should contain a corresponding time unit (e.g., 1s, 2m, 3h). A value of zero (the default) means no timeout.")
	watchCmd.Flags().Float64Var(&statusCmdArgs.unitTimeoutFactor, "unit-timeout-factor", 1, "Factor to scale the unit timeout. A value of one (the default) means no scaling.")
	watchCmd.Flags().StringVar(&statusCmdArgs.record, "record", "", "record all API events in a file, used to generate test files")
	watchCmd.Flags().StringVar(&statusCmdArgs.save, "save", "", "save a report of watched flux resources in this file, as HTML")
	watchCmd.Flags().StringArrayVar(&statusCmdArgs.exitCondition, "exit-condition", []string{}, "Stop waiting and exit if a resource matches any of the status conditions")
	watchCmd.Flags().DurationVar(&statusCmdArgs.retryTimeout, "retry-backoff-timeout", 10*time.Millisecond, "Duration for the base delay of the backoff timeout for the retries of the first API connection")

	rootCmd.AddCommand(watchCmd)
	log.SetFlags(log.Ldate | log.Lmicroseconds)
}

func watchCmdRun(cmd *cobra.Command, args []string) error {

	var watchedResource string
	if len(args) >= 1 {
		watchedResource = args[0]
	} else {
		watchedResource = ""
	}
	if statusCmdArgs.unitTimeout != 0 {
		statusCmdArgs.reconcile = true
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	if statusCmdArgs.timeout != 0 {
		ctx, cancel = context.WithTimeout(ctx, statusCmdArgs.timeout)
	}
	defer cancel()

	cfg, err := kubeconfigArgs.ToRESTConfig()
	if err != nil {
		return err
	}
	// Disable api deprecation warnings
	cfg.WarningHandler = rest.NoWarnings{}

	exitConditions, err := parseExitCondition(statusCmdArgs.exitCondition)
	if err != nil {
		return fmt.Errorf("failed to parse exit conditions: %v", err)
	}

	objUpdate := make(chan informer.ObjEvent, 1000)
	fluxInformer, err := informer.NewInformer(ctx, cfg, objUpdate, watchedResource, statusCmdArgs.reconcile,
		statusCmdArgs.resumeSuspended, statusCmdArgs.retryTimeout, *kubeconfigArgs.Namespace, statusCmdArgs.record)
	if err != nil {
		return err
	}
	defer fluxInformer.Stop()

	if statusCmdArgs.log || !spinner.IsTerminal() {
		err = logResources(ctx, fluxInformer, objUpdate, watchedResource, exitConditions)
	} else {
		log.SetOutput(io.Discard)
		sm := spinner.NewSpinnerManager(ctx)
		sm.Start()
		err = updateSpinners(ctx, fluxInformer, sm, objUpdate, watchedResource, exitConditions)
		sm.Stop()
	}

	if err == timeoutError {
		progressUpdate, _ := computeProgressUpdate(fluxInformer.ObjectMap, watchedResource)
		fmt.Println("Timed-out waiting for the following resources to be ready:")
		timeoutSummary(cfg, progressUpdate, statusCmdArgs.skipInventory)
		cmd.SilenceErrors = true // We want to fail with non-zero exit code, but don't want print the error
		return err
	} else if err != nil {
		return err
	}

	if statusCmdArgs.save != "" {
		if err := saveObjects(fluxInformer.ObjectMap.FilterDependencies(watchedResource), statusCmdArgs.save); err != nil {
			return err
		}
	}
	return nil
}

func parseExitCondition(userConditions []string) ([]map[string]string, error) {
	var parsedConditions []map[string]string

	for _, condition := range userConditions {
		parsedCondition := strings.SplitN(condition, "=", 2)
		if len(parsedCondition) != 2 {
			return nil, fmt.Errorf("exit condition must be in 'key=value' format: %s", condition)
		}

		key := parsedCondition[0]
		value := parsedCondition[1]

		// Check if the key is either 'reason' or 'message'
		if key != "reason" && key != "message" {
			return nil, fmt.Errorf("exit condition must start with 'reason=' or 'message=': %s", condition)
		}

		// Remove potential surrounding quotes from value
		value = strings.Trim(value, "\"")
		parsedConditions = append(parsedConditions, map[string]string{key: value})
	}

	// If no user conditions are specified, append default conditions
	if len(parsedConditions) == 0 {
		parsedConditions = append(parsedConditions,
			map[string]string{"message": "values don't meet the specifications of the schema"},
			map[string]string{"message": "install retries exhausted"},
			map[string]string{"message": ": execution error at "},
		)
	}
	return parsedConditions, nil
}

func checkConditions(informerTree *informer.TreeInformer, obj informer.Node, exitConditions []map[string]string) error {
	if !informerTree.Reconcilable(obj) || !obj.HasHandledReconcileAnnotation() {
		return nil
	}
	for _, condition := range obj.GetConditions() {
		if condition.ObservedGeneration == obj.GetGeneration() {
			for _, exitCond := range exitConditions {
				for key, expectedValue := range exitCond {
					actualValue := ""
					if key == "reason" {
						actualValue = condition.Reason
					} else if key == "message" {
						actualValue = condition.Message
					}
					// Check for exact match for keys other than "message"
					if key != "message" && actualValue == expectedValue {
						return fmt.Errorf("Exiting as condition \"%s\" equals \"%s\" in unit %s, message: %s\n", key, expectedValue, obj.DisplayName(), condition.Message)
					}
					// For "message" key, check if actualValue contains expectedValue
					if key == "message" && strings.Contains(actualValue, expectedValue) {
						return fmt.Errorf("Exiting as condition \"%s\" contains \"%s\" in unit %s, message: %s\n", key, expectedValue, obj.DisplayName(), condition.Message)

					}
				}
			}
		}
	}
	return nil
}

func computeProgressUpdate(objMap informer.ObjectMap, watchedResource string) (nodeMap, bool) {
	allComplete := true
	progressUpdate := nodeMap{}
	for _, obj := range objMap.FilterDependencies(watchedResource) {
		if obj.Reconciled() || statusCmdArgs.ignoreSuspended && obj.Suspended() {
			continue
		}
		allComplete = false
		if showResource(obj, objMap) {
			progressUpdate[obj.QualifiedName()] = obj
		}
	}
	return progressUpdate, allComplete
}

func logSummary(progressUpdate nodeMap) {
	maxNameLength := 0
	for _, res := range progressUpdate {
		maxNameLength = max(maxNameLength, len(res.QualifiedName()))
	}
	formatString := fmt.Sprintf(" │  %%-%ds  %%s", maxNameLength)
	for _, res := range progressUpdate {
		log.Printf(formatString, res.DisplayName(), res.Status())
	}
}

func logResources(ctx context.Context, i *informer.TreeInformer, updates chan informer.ObjEvent, watchedResource string, exitConditions []map[string]string) error {
	objMap := i.ObjectMap
	sortedMap := objMap.SortDependencies(objMap.FilterDependencies(watchedResource))
	progressing := nodeMap{}
	allComplete := true

	for _, obj := range sortedMap {
		if err := checkConditions(i, obj, exitConditions); err != nil {
			return err
		}
		if !(obj.Reconciled() || statusCmdArgs.ignoreSuspended && obj.Suspended()) {
			allComplete = false
		}
		if !showResource(obj, objMap) {
			continue
		} else if obj.QualifiedName() == watchedResource && obj.Reconciled() {
			log.Printf("All Done: resource %s is ready!", watchedResource)
			return nil
		}

		log.Printf("%-50s %s", obj.DisplayName(), obj.Status())
	}
	// FIXME: should we ensure that watched resource is present in cluster?
	if allComplete {
		if watchedResource == "" {
			log.Println("All Done: all resources are ready!")
			return nil
		} else {
			return fmt.Errorf("Flux object %s not found in this cluster", watchedResource)
		}
	}
	for {
		select {
		case <-ctx.Done():
			switch ctx.Err() {
			case context.DeadlineExceeded:
				log.Println("Command timeout exceeded")
				return timeoutError
			case context.Canceled:
				log.Println("Command was interrupted")
				return nil
			default:
				return ctx.Err()
			}
		case <-ticker.C:
			if statusCmdArgs.unitTimeout > 0 {
				if err := checkUnitsTimeouts(i); err != nil {
					log.Printf("Unit timeout exceeded: %s", err)
					return timeoutError
				}
			}
		case event := <-updates:
			objName := event.FluxObject.QualifiedName()
			if event.Delete {
				log.Printf("%s has been deleted", objName)
				continue
			}
			// Retrieve node handling flux object
			obj, found := objMap.Get(objName)
			if !found {
				log.Printf("Warning: no treeNode found for object %s (status: %s)", objName, event.FluxObject.Status())
				continue
			}
			if watchedResource == "" {
				log.Printf("%s state changed: %s", obj.DisplayName(), obj.Status())
			} else {
				// When watchedResource is used, only display events related to the resource and its dependencies
				if obj.QualifiedName() == watchedResource {
					if obj.Reconciled() {
						log.Printf("All Done: resource %s is ready!", watchedResource)
						return nil
					} else {
						log.Printf("%s state changed: %s", obj.DisplayName(), obj.Status())
					}
				} else {
					if objMap.DependsOn(watchedResource, obj.QualifiedName()) {
						log.Printf("%s state changed: %s", obj.DisplayName(), obj.Status())
					} else {
						continue
					}
					// Object status change may have impacted the status of watchedResource
					if watched, found := objMap.Get(watchedResource); found {
						if watched.Reconciled() {
							log.Printf("All Done: resource %s is ready!", watchedResource)
							return nil
						}
					}
				}
			}
			if err := checkConditions(i, obj, exitConditions); err != nil {
				return err
			}
			progressUpdate, allComplete := computeProgressUpdate(objMap, watchedResource)
			if allComplete && watchedResource == "" {
				return nil
			} else if len(progressUpdate) > 0 && !sameObjectNames(progressing, progressUpdate) {
				log.Println(" ╭╴Waiting for the following resources to progress:")
				logSummary(progressUpdate)
				progressing = progressUpdate
				log.Println(" ╰╴╴╴┄")
			}
			if statusCmdArgs.once {
				return nil
			}
		}
	}
}

func updateSpinners(ctx context.Context, i *informer.TreeInformer, sm spinner.SpinnerManager,
	updates chan informer.ObjEvent, watchedResource string, exitConditions []map[string]string) error {
	objMap := i.ObjectMap
	for {
		sortedMap := objMap.SortDependencies(objMap.FilterDependencies(watchedResource))
		index := 0
		allComplete := true
		// First list resource that are Ready
		for _, obj := range sortedMap {
			if err := checkConditions(i, obj, exitConditions); err != nil {
				return err
			}
			if !(obj.Reconciled() || statusCmdArgs.ignoreSuspended && obj.Suspended()) {
				allComplete = false
				continue
			} else if obj.QualifiedName() == watchedResource && obj.Reconciled() {
				updateSpinner(obj, sm, index)
				sm.AddSpinner(fmt.Sprintf("All Done: resource %s is ready!", watchedResource)).SetStatus(spinner.Complete)
				return nil
			}
			if !(statusCmdArgs.ignoreSuspended && obj.Suspended()) {
				updateSpinner(obj, sm, index)
				index = index + 1
			}
		}
		// Then list resources that are progressing
		for _, obj := range sortedMap {
			if !showResource(obj, objMap) || obj.Ready() {
				continue
			}
			updateSpinner(obj, sm, index)
			index = index + 1
		}
		// Clear extra spinners if any
		sm.SetSpinnersCount(index)
		if allComplete {
			if watchedResource == "" {
				sm.AddSpinner("All Done!").SetStatus(spinner.Complete)
				return nil
			} else {
				return fmt.Errorf("Flux object %s not found in this cluster", watchedResource)
			}
		}
		if statusCmdArgs.once {
			return nil
		}
		select {
		case <-ctx.Done():
			switch ctx.Err() {
			case context.DeadlineExceeded:
				sm.AddSpinner("Command timeout exceeded").SetStatus(spinner.Error)
				return timeoutError
			case context.Canceled:
				sm.AddSpinner("Command was interrupted").SetStatus(spinner.Error)
				return nil
			default:
				return ctx.Err()
			}
		case <-updates:
			continue
		case <-ticker.C:
			if statusCmdArgs.unitTimeout > 0 {
				if err := checkUnitsTimeouts(i); err != nil {
					sm.AddSpinner(fmt.Sprintf("Unit timeout exceeded: %s", err)).SetStatus(spinner.Error)
					return timeoutError
				}
			}
		}
	}
}

func updateSpinner(object informer.Node, sm spinner.SpinnerManager, index int) {
	var sp *spinner.Spinner
	if index < len(sm.GetSpinners()) {
		sp = sm.GetSpinners()[index]
	} else {
		sp = sm.AddSpinner("")
	}
	if object.Ready() {
		if msg, ok := object.GetAnnotations()[ReadyMessageAnnotation]; ok {
			sp.UpdateMessage(fmt.Sprintf("%s - Resource is ready: %s", object.DisplayName(), msg))
		} else {
			sp.UpdateMessage(fmt.Sprintf("%s - Resource is ready", object.DisplayName()))
		}
		sp.SetStatus(spinner.Complete)
	} else {
		sp.UpdateMessage(fmt.Sprintf("%s - %s", object.DisplayName(), strings.Replace(object.Status(), "\n", "", -1)))
		if object.Stalled() {
			sp.SetStatus(spinner.Error)
		} else if object.Suspended() {
			sp.SetStatus(spinner.Suspended)
		} else {
			sp.SetStatus(spinner.Progressing)
		}
	}
}

func showResource(obj informer.Node, objMap informer.ObjectMap) bool {
	if statusCmdArgs.ignoreSuspended && obj.Suspended() || obj.HasMissingDependencies() {
		return false
	}
	for _, dep := range objMap.GetDependencies(obj.QualifiedName()) {
		if !dep.Reconciled() {
			return false // don't show objects if their dependencies are not ready
		}
	}
	return true
}

func checkUnitsTimeouts(i *informer.TreeInformer) error {
	for _, node := range i.ObjectMap.Items() {
		if node.Ready() || !i.Reconcilable(node) {
			continue
		}
		nodeTimeout := statusCmdArgs.unitTimeout
		if customTimeout, err := time.ParseDuration(node.GetAnnotationValue(UnitReconcileTimeout)); err == nil {
			nodeTimeout = customTimeout
		}
		referenceNode := node
		if timeoutReference := node.GetAnnotationValue(UnitTimeoutReference); timeoutReference != "" {
			reference, found := i.ObjectMap.Get(timeoutReference)
			if !found {
				return fmt.Errorf("timeoutReference %s defined in unit %s not found in cluster", timeoutReference, node.QualifiedName())
			}
			referenceNode = reference
		}
		referenceTimeout, err := time.ParseDuration(referenceNode.GetAnnotationValue(UnitReconcileTimeout))
		if err != nil {
			referenceTimeout = statusCmdArgs.unitTimeout
		}
		if nodeTimeout < referenceTimeout {
			return fmt.Errorf("unit %s has a smaller timeout (%s) than its timeoutReference %s (%s)",
				node.DisplayName(), nodeTimeout, referenceNode.DisplayName(), referenceTimeout)
		}

		nodeTimeout = time.Duration(float64(nodeTimeout) * statusCmdArgs.unitTimeoutFactor)
		if i.CurrentReconcileDuration(referenceNode.GetObject()) > nodeTimeout {
			return fmt.Errorf("unit %s did not became ready after %s", node.DisplayName(), nodeTimeout)
		}
	}
	return nil
}

func saveObjects(nodeList informer.NodeList, fileName string) error {

	units := []interface{}{}
	for _, node := range nodeList {
		objCopy := node.ClientObject().DeepCopyObject().(client.Object)
		objCopy.SetManagedFields([]metav1.ManagedFieldsEntry{})
		units = append(units, objCopy)
	}
	return report.Generate(units, fileName)

}

func timeoutSummary(clientConfig *rest.Config, stalledMap nodeMap, skipInventory bool) {
	if skipInventory {
		for _, res := range stalledMap {
			fmt.Printf("    * %s: %s\n", res.DisplayName(), res.Status())
		}
	} else {
		// Build list of stalled objects
		stalledObjs := []client.Object{}
	OUTER:
		for _, res := range stalledMap {
			// Exclude HelmReleases produced by a Kustomization to avoid duplicates
			if res.GetObjectKind().GroupVersionKind().Kind == helmv2.HelmReleaseKind {
				for _, other := range stalledMap {
					if other.GetObjectKind().GroupVersionKind().Kind == kustomizev1.KustomizationKind &&
						other.GetNamespace() == res.GetNamespace() &&
						other.GetName() == res.GetName() {
						continue OUTER
					}
				}
			}
			stalledObjs = append(stalledObjs, res.ClientObject())
		}
		inventory.PrintResourcesStatus(context.Background(), inventory.InventoryOptions{SkipReadyResources: true}, clientConfig, stalledObjs...)
	}
}

// Check if the two maps contain the same objects names
func sameObjectNames(m nodeMap, n nodeMap) bool {
	if len(m) != len(n) {
		return false
	}
	for k := range m {
		if _, ok := n[k]; !ok {
			return false
		}
	}
	return true
}
