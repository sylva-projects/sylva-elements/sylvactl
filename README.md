# sylvactl

This client intends to provide tools to help the operations on sylva stack.

For now it only supports the watch command that can be used to follow the progress of the reconciliation of flux objects in sylva stack.
This command builds a dependency tree of flux resources, and focuses the output on resources that are currently progressing (it hides periodic reconciliations of resources that are ready, as well as resources that have dependencies that are not ready)

In the future, we intend to add various features like:
- Set-up & configure the sylva-units HelmRelease (final goal will probably be to replace current bootstrap.sh & apply.sh of sylva-core project)
- Provide more features to ease the operation of the stack (for example, we could inspect resources that are managed flux and provide details of encountered errors if any)
- Speed up flux convergence by triggering reconciliation of objects as soon as their dependencies become ready
- Measure the progress of each flux resource reconciliation, and timeout if it takes too much time
- Use a standard client to fail promptly if there is an API connectivity issue (informers errors are not easy to catch)

There is also probably a fair amount of refactoring to perform, this is a very first functional implementation.
We should define an appropriate interface for tree objects, leveraging as much as possible flux2/cmd/flux/object.